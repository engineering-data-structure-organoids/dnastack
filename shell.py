"""Execute shell commands

Ben Shirt-Ediss
"""

import subprocess


def execute(cmd) :

	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
	(output, err) = p.communicate()
