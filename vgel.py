"""VIRTUAL GEL IMAGE - for stack washing model with straight bricks

Gel bands are displayed approximately as lines whose thickness depends on the band mass concentration. No ring bands.

NOTE: Image Magick must be installed and shell command "mogrify" must be available
Image Magick is used for rotating images -- its complex to do in native python

Ben Shirt-Ediss, August 2017-June 2019
"""

import constants
import shell

import sys
import matplotlib.pyplot as plt
import math
import os.path
import pickle




# --------------------- User Settings -----------------------

def exact_running_bp(species_group) :
	"""
	For ssDNA monomers in groups gz, g0 or g1
	Exact running bp used
	"""

	if species_group == "gz" :
		return 33.0  # releaser z and linker-releaser helix
	if species_group == "g0" :
		return 49.0  # s
	if species_group == "g1" :
		return 55.0  # p and w
	else :
		return 0

	# p migrates at 57 bp
	# w migrates at 54 bp, so I return the average, they are in the same grouping



def estimated_running_bp(species_group) :
	"""
	For linear complexes of 2 strands or more, with ssDNA overhangs, (i.e. in g2 or above)
	Running bp estimated by linear function, so arbitrarily long complexes can have running bp estimated
	"""

	group_num = int(species_group[1:])

	nt = group_num * 56

	running_bp = (7.03277e-01 * nt) + 3.48125e+01

	return running_bp





# LADDER to display the virtual gel - NEB low molecular weight ladder
# lowest rung must not be bigger than 950 pixels


LADDER = 		{	766: 250,
					500: 292,
					350: 318,
					300: 340,
					250: 360,
					200: 382,
					150: 413,
					100: 446,
					75:  475,
					50:  526,
					25:  658 }

	

# control how gel bands are displayed

MAX_XLANE = 1000							# gel images plotted on scale 0 to 1000
MARGIN_XLANE = 50       					# first LADDER band (25) appears at 50, last LADDER band (766) appears at 950 

MASS_CONC_TO_LINEWIDTH_MULTIPLIER = 1		# relationship between line thickness and mass conc

CAP_LINEWIDTH = False

MAX_LINEWIDTH = 10							# If CAP_LINEWIDTH is true, bands dont get thicker, after this maximum linewidth
											# then they are displayed as purple

# ------------------------------------------------------------


#
#
# 1. Render gel -- helper functions
#
#


def inverse_standard_curve(LADDER, bp) :
	"""
	bp to y-pixels from the top of the image, for the LADDER on the image supplied
	"""

	min_bp = min(LADDER)
	max_bp = max(LADDER)

	if bp < min_bp or bp > max_bp :
		return -1
	if bp == min_bp :
		return LADDER[bp]

	# read LADDER keys and values into 2 lists
	list_bp = []
	list_ypixel = []
	for key in sorted(LADDER) :
		list_bp.append(key)					# bp's go ascending in value
		list_ypixel.append(LADDER[key])		# pixels go descending in value

	# find what two values bp lies between
	# and do linear interpolation
	ypixel = -1
	for i in range(0, len(list_bp)) :
		
		if(i < len(list_bp)-1) :	# ensure there is a pair
			if(bp > list_bp[i] and bp <= list_bp[i+1]) :
				
				dy_dx = (list_ypixel[i+1] - list_ypixel[i]) / (list_bp[i+1] - list_bp[i])
				ypixel = list_ypixel[i] + (dy_dx * (bp - list_bp[i])) 
				break

	return ypixel




def ypixel_to_xlane(ypixel, LADDER) :
	"""
	Converts a y pixel value (from top of gel image)
	into an x value from the right of gel graph
	"""

	ypixel_min = min(LADDER.values())
	ypixel_max = max(LADDER.values())

	# shift LADDER, so highest rung (766) occurs at 0 ypixels
	n1 = ypixel - ypixel_min

	# stretch LADDER, so lowest rung (25) will appear at 980 pixels
	n2 = n1 * ( (MAX_XLANE - MARGIN_XLANE - MARGIN_XLANE - 0) / (ypixel_max - ypixel_min) )

	# translate LADDER 20 pixels from origin
	n3 = n2 + MARGIN_XLANE

	# invert LADDER, so highest rung (766) occurs at 990 pixels
	# and lowest rung (25) occurs at 10 pixels
	n4 = MAX_XLANE - n3

	return n4




def ng_per_ul_to_line_width(ngul) :

	lw = MASS_CONC_TO_LINEWIDTH_MULTIPLIER * ngul
	
	lwmax = False
	if lw > MAX_LINEWIDTH and CAP_LINEWIDTH : 
		lw = MAX_LINEWIDTH
		lwmax = True

	return lwmax, lw







#
#
# 2. Render single gel image
#
#
#


def render_bands(lane_gelbands, virtualgel_filename, data_filename) :
	"""
	Renders virtual gel image, and accompanying data file about gel bands
	
	Band super-positioning is taken account of

	lane_gelbands is a dictionary, where key is lane number, and maps to gelbands dictionary for that lane

	1 : "Ladder"
	2 : {"g0": (mass_conc, mass conc corrected for SYBR gold intercalation), "g1": (mass_conc, mass conc corrected for SYBR gold intercalation)} 
	3 : "Empty"

	"Ladder" means display ladder in this lane
	"Empty" means display no data in this lane
	lane_num is ALWAYS from 1 to 10

	The gel image is saved as virtualgel_filename

	The accompanying text data file lists species groups (gel bands) in 
	decreasing mass concentration, and each is labelled with approx running bp
	"""

	bandfile = open(data_filename, "w")

	plt.figure(1, figsize=(7,5))	# has same width as ring bar graph below

	for lanenum in range(1,11) :

		gelbands = lane_gelbands[lanenum]

		plt.subplot(10,1,lanenum)
		axes = plt.gca()
		axes.yaxis.set_visible(False)
		axes.xaxis.set_visible(False)		
		axes.set_xlim([0, MAX_XLANE])

		# write lane number at the top of lane
		plt.text(MAX_XLANE-35, 0.3, lanenum, rotation=270, fontsize=10)

		if gelbands == "Ladder" :

			for bp in LADDER :
				x = ypixel_to_xlane(inverse_standard_curve(LADDER, bp), LADDER)
				plt.plot([x, x],[0, 3], 'k', linewidth=3)
				# bp text label for ladder band
				plt.text(x, 0.8, str(bp), rotation=270, fontsize=6)				

		elif gelbands != "Empty" :

			g2ngul = {}; g2bp = {} # for the band file in (2) below

			# 1. DRAW GEL LANE BANDS
			# loop through all species groups
			for species_group in gelbands :

				(ngul, ngul_SYBR) = gelbands[species_group]

				if species_group in ["gz", "g0", "g1"] :
					# these groups have exact bp assigned
					bp = exact_running_bp(species_group)
				else :
					# polymer of 2 strands or more - has bp estimated from nt of group by best fit line
					bp = estimated_running_bp(species_group)

				# for the band file in (2) below
				g2ngul[species_group] = ngul_SYBR; g2bp[species_group] = bp

				# if concentration is above 0, and band is within ladder range, draw the band
				# (very low conc bands will not show because of tiny line width)
				if ngul_SYBR > 0 and (bp >= min(LADDER) and bp <= max(LADDER)) :
						
					lwmax, lw = ng_per_ul_to_line_width(ngul_SYBR)
					x = ypixel_to_xlane(inverse_standard_curve(LADDER, bp), LADDER)
					
					if not lwmax :
						plt.plot([x, x],[0, 1], 'k', linewidth=lw)
					else :
						# lines that have a capped thickness are coloured in magenta
						plt.plot([x, x],[0, 1], 'm', linewidth=lw)

			# 2. WRITE ORDERED SPECIES GROUP CONCS TO FILE
			bandfile.write("\n\n==================================\n")
			bandfile.write("LANE %d\n" % (lanenum))
			bandfile.write("==================================\n\n")
			bandfile.write("group\t\tng/ul\t\tgel running bp\n\n")

			# sort dictionary by descending values: https://stackoverflow.com/questions/20944483/python-3-sort-a-dict-by-its-values/20948781
			s = [(k, g2ngul[k]) for k in sorted(g2ngul, key=g2ngul.get, reverse=True)]
			for k, v in s:
				# prints group key (k) ordered by decreasing mass concentration value (v)
				bandfile.write("%s\t\t%.4e\t\t%3.2f\n" % (k, v, g2bp[k]))

	bandfile.close()

	plt.savefig(virtualgel_filename, bbox_inches='tight')
	plt.clf()
	plt.close()

	# rotate image by 90 degrees
	cmd = "mogrify -rotate 270 %s\n" % (virtualgel_filename)
	shell.execute(cmd)










#
#
# 0. Prepare stocal model output for rendering as gel
#
# Final state converted to gel band groupings, with cumulated mass concentration
#
# The SYBR gold attachment model used is the simplest -- 
#	SYBR gold intercalation is just proportional to the mass concentration of DNA, regardless of the percentage ssDNA and dsDNA
#	And 'start' brick is the exception, with 0 SYBR gold attachment
#



def species_to_band_grouping(species) :
	"""
	Returns the band grouping of a species

	Returns -1 if species cannot be put in a band grouping

	Band groupings are necessary because, in the gel, different species run at the same same in the same band
	"""
	
	strands = len(species)

	is_discounted = (species == "IkzI")
		# Linker-releaser is a dsDNA which remains BOUND to the beads. It never shows up on gel electrophoresis images

	is_tau_stack_ending_start = ((len(species) > 1) and species.endswith("s"))

	is_ring = False
	if strands > 3 :
		se = "%s%s" % (species[0], species[-1])
		is_ring = (se in ["pw", "wp", "rq", "qr"])

	

	if is_discounted or is_tau_stack_ending_start or is_ring :
		return -1
	if species == "z" :
		return "gz"						# gz is releaser group, migrating at 33bp
	elif species == "s" :
		return "g0"						# g0 is start group, migrating at 49bp
	elif species in ["IwrI", "IpqI"] :
		return "g1"						# g1 is single ssDNA monomers, plus double strand pq and rw signals, migrating at 55bp
	else :
		# group number is equal to number of strands in complex
		# migration time is determined by linear equation
		return "g%d" % (len(species))




def stocal_output_to_lane_gelbands(gelname, supernatant_to_display) :
	"""
	Returns lane_gelbands dictionary
	"""

	"""
	Data structures reminder:

	lane_gelbands = {	1: "Ladder",	
						2: "Empty", 
						3: "Empty", 
						4: {"g0": (100, 2, 2)}, 		# a gelbands dictionary
						5: {"g0": (200, 2, 5)}, 		# a gelbands dictionary
						6: "Empty", 
						7: "Empty", 
						8: "Empty", 
						9: "Empty", 
						10: "Ladder" }

	gelbands =		 	{"g0": (100, 2, 2),
						 "g1": (150, 0.1, 0.1)}
	"""	


	lane_gelbands = {}

	for lanenum in range(1,11) :

		fname = "results/sim_%s_%d.pkl" % (gelname, lanenum)

		if lanenum == 1 :
			lane_gelbands[lanenum] = "Ladder"
		else :
			# see if a simulation was done for this gel lane
			if os.path.isfile(fname) :

				gelbands = {}

				f = open(fname, "rb")
				simdata = pickle.load(f)
				f.close()

				# get correct supernatant solution to display
				total_bricknum = len(simdata["bricks_add_order"]) - 1
				endstate = simdata["supernatant%d" % total_bricknum]
				if supernatant_to_display == "releaser" :
					endstate = simdata["supernatant_releaser%d" % total_bricknum]	
				
				# now convert the endstate to gel bands
				for species in endstate :

					particles, nM = endstate[species]					# particles and nanomolar conc, from stocal end state
					
					# string replace all X for w (X is used in place for w, in constants.py)
					species = species.replace("X", "w")

					# get grouping and mass conc of this species				
					g = species_to_band_grouping(species)

					if g == -1 : continue  # ignore this species

					ngul = constants.nM_to_ng_per_ul(nM, species)		# approx mass conc
					ngul_SYBR = ngul
					
					if species == "s" : ngul_SYBR = ngul * 0.1			# start only shows faintly with SYBR Gold staining
				
					if gelbands.get(g, -1) == -1 :
						# this grouping does not exist, create it
						gelbands[g] = (ngul, ngul_SYBR)
					else :
						# species already exist in this group, add to them
						ngul0, ngul_SYBR0 = gelbands[g]
						gelbands[g] = (ngul0 + ngul, ngul_SYBR0 + ngul_SYBR)

				lane_gelbands[lanenum] = gelbands

			else :
				# no simulation was done of this lane
				lane_gelbands[lanenum] = "Empty"

	return lane_gelbands






	

#
#
#
# Get simulation data
#
#


if __name__ == "__main__" :

	if len(sys.argv) == 3 :

		gelname = str(sys.argv[1])	
		supernatant_to_display = str(sys.argv[2])	

		if supernatant_to_display not in ["supernatant", "releaser"] :
			print("Supernatant type should be 'supernatant' or 'releaser'")
			quit()

		lane_gelbands = stocal_output_to_lane_gelbands(gelname, supernatant_to_display)
		render_bands(lane_gelbands, "results/%s_%s.png" % (gelname, supernatant_to_display), "results/%s_%s.txt" % (gelname, supernatant_to_display))

	else :
		print("Supply two arguments: name of gel, and name of final solution ('supernatant' or 'releaser')")





