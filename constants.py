"""Constants for DNA stack washing model

Ben Shirt-Ediss
"""


from numpy.random import rand




#
#
# CORE MODEL PARAMETERS
#
#



# kinetic rate constants (approx values fitted from UV-Vis data)

KA = 	3.0 * (10**4) 	# M^-1 s^-1
KBC = 	3.0 * (10**4)	# M^-1 s^-1
KABC =  2.5 * (10**5) 	# M^-1 s^-1


# percent bead loss on each washing step (fitted parameter)

MU = 	0.10									# percent, 0 = 0%, 1 = 100%


# percentage of species in supernatant carried over to next cycle (fitted parameter)
# 0 means PERFECT washing with no residual species, 1 means NO washing
# PHI = 0.05 means that 5% of supernatant species are carried over when bead_mass is its initial mass
# With no beads, no species are carried through the washing step.

PHI = 	0.33									# percent, 0 = 0%, 1 = 100%



# for faster simulations, just to produce paper figures
# leave as False

STOP_WHEN_POPPED_SIGNALS_INDISTINGUISHABLE = False


# concentration difference between popped Xr and Yr signals, complex in excess to count as a "clear signal"

EPSILON_XY = 10			# nM







#
#
# SECONDARY PARAMETERS
#
#


# small sim volume for manageable particle numbers (at 0.5 picolitre, stochastic sims take too long to complete)

VOL = 1.5e-13									# litres  (0.15 picolitres: 1e-13 litre = 0.1 picolitre) 	

# Avogadro

NA = 6.02214076e23								# mol^-1


# linker & releaser	(they are assumed to be at exact concs, to simplify)

LINKER_CONC = 200e-9							# M

RELEASER_CONC = 200e-9							# M

RELEASER_WAIT_TIME = 30 * 60					# s 	# 30 min


# stochastic rate constants
# (note: rate constants are corrected by stocal, for the case that both reactants are identical)

C_KA = 	KA / (NA * VOL) 		
C_KBC = KBC / (NA * VOL)
C_KABC = KABC / (NA * VOL)

C_K1 = C_KA
C_K2 = C_KBC






#
#
# CONCENTRATION CONVERSIONS
#


MONOMERS = 		{	's':	'CACACTATTTCCCTTCTACCCGCCCTATCTCATCTCTCATCTCATCTTAA',
					'p':	'ATAGGGCGGGTAGAAGGGAAATAGTGTGATCCAGTTATTATAGTTTTGAAGCGTAT',
					'w':	'CACACTATTTCCCTTCTACCCGCCCTATATACGCTTCAAAACTATAATAACTGGAT',
					'X':	'CACACTATTTCCCTTCTACCCGCCCTATATACGCTTCAAAACGTGCAACAGTCGACTAAAAAAAACTGCTTGTATCTGCCCATACTGTTGCACTATAATAACTGGAT',
					'Y':	'CACACTATTTCCCTTCTACCCGCCCTATATACGCTTCAAAACGCCTTGCGTGCGCCTACCTCGAAATTCACCACCCCCACCTCTCTTTTATATCCCACATTTCGAGGTAGGCGCACGCAAGGCTATAATAACTGGAT',
					'r':	'ATCCAGTTATTATAGTTTTGAAGCGTATATAGGGCGGGTAGAAGGGAAATAGTGTG',
					'q': 	'ATACGCTTCAAAACTATAATAACTGGATCACACTATTTCCCTTCTACCCGCCCTAT',
					'k':	'GAGAGAGATGATTAAGATGAGATGAGAGATGAG',	 # linker
					'z':	'CTCATCTCTCATCTCATCTTAATCATCTCTCTC'}  	 # releaser


def nt_in_species(species) :

	nt = 0
	for char in species :
		if char not in ["I"] :		# don't count blunt end (I) markers
			nt += len(MONOMERS.get(char,''))

	return nt



def nM_to_ng_per_ul(nM, species) :
	"""
	(Approximate : assumes average weight of DNA nucleotide as 330g per mole)
	"""

	nt = nt_in_species(species)

	# set small negative molar concentrations to 0 mass concentration
	# Note: negative concentrations result from ODE integration overshoot, and are usually negligible
	if nM < 0 :
		return 0
	else :
		return nM * (330 * nt) * (1 / 1e6)







#
# FITTING PHI AND MU TO EXPERIMENTAL GELS, VIA INTENSITY ORDERINGS PROCEDURE
#
#


# the range of mass concentration, whereby you cannot distinguished two bands as being different intensities

EPSILON = 0.05				# ng/ul


# manually curated from experimental gels

INTENSITY_ORDERINGS = 	{	"gel33_2" :	"g5 > g7 > g3 > g9 > g11 = g2 = g1 > g13 > g15 > *",
							"gel33_3" :	"g4 > g6 > g2 > g8 > g1 > g10 > g12 > *",
							"gel33_4" :	"g3 > g5 > g7 > g9 = g1 > g11 > g13 > *",
							"gel33_5" :	"g4 > g2 > g6 > g1 > g8 > g10 > *",

							"gel43_2" :	"g2 > *",
							"gel43_3" :	"g3 > g5 > g7 > g9 = g2 = g1 > *",
							"gel43_4" :	"g4 > g6 > g8 > g2 > g10 = g1 > *",
							"gel43_5" :	"g5 > g7 > g9 > g3 > g11 > g2 = g1 > *",
							"gel43_6" :	"g6 > g8 > g10 > g4 > g12 > g2 > g1 > g14 > *",
							"gel43_7" :	"g7 > g9 > g11 > g13 = g5 > g4 > g15 = g2 = g1 > *",	

							"gel64_2" :	"g2 > *",
							"gel64_3" :	"g3 > g5 > g7 > *",
							"gel64_4" :	"g5 > g7 > g9 > g3 = g11 > g13 > *",
							"gel64_5" :	"g7 > g9 > g11 > g13 = g5 > g3 = g15 > *",
							"gel64_6" :	"g5 > g7 > g9 > g3 > g11 > g13 > g15 > *",
							"gel64_7" :	"g3 = g5 > g7 = g9 > g11 > g13 > *",
							"gel64_8" :	"g4 > g6 > g2 > g8 > g10 > g3 > *"	}








#
#
# NANODROP STOCK CONCENTRATION CORRECTIONS
#
#

def correct_concentrations(bricks_add_order, bricks_add_conc, bricks_wait_time, use_concs="Dec2018") :
	"""
	We did nanodrop on all the prepared stock solutions. 

	This converts the conc we thought we were pipetting, the "nominal" concentration,
	into the actual conc pipetted, according to nanodrop.

	Pipetting error is not taken into account.
	"""

	# averaged Nanodrop One values of stock concs of "old" bricks ordered May 2017
	# all done for 10 repeats. Nunzia supplied these to me in Jan 2019. The nominal concentration of the stocks is 10uM.
	s_nanodrop = 9.2e-6			# M
	p_nanodrop = 12.16e-6		# M
	w_nanodrop = 10.54e-6		# M
	X_nanodrop = 11.3e-6		# M
	Y_nanodrop = 8.61e-6		# M
	r_nanodrop = 9.02e-6		# M
	q_nanodrop = 5.39e-6		# M 		only half the concentration it is meant to be!
	k_nanodrop = 9.87e-6		# M
	z_nanodrop = 10.12e-6		# M

	if use_concs=="Dec2018" :
		# averaged Nanodrop One values of stock concs of "new" bricks re-ordered Dec 2018
		# all done for 10 repeats. Nunzia supplied these to me in Jan 2019. The nominal concentration of the stocks is 10uM.
		s_nanodrop = 9.54e-6		# M
		p_nanodrop = 10.94e-6		# M
		w_nanodrop = 10.52e-6		# M
		X_nanodrop = 9.42e-6		# M
		Y_nanodrop = 8.92e-6		# M
		r_nanodrop = 9.72e-6		# M
		q_nanodrop = 9.05e-6		# M
		k_nanodrop = 9.79e-6		# M
		z_nanodrop = 9.79e-6		# M


	for i, brick in enumerate(bricks_add_order) :

		if brick == "s" :
			bricks_add_conc[i] = bricks_add_conc[i] * (s_nanodrop / 10000e-9)
		if brick == "p" :
			bricks_add_conc[i] = bricks_add_conc[i] * (p_nanodrop / 10000e-9)
		if brick == "w" :
			bricks_add_conc[i] = bricks_add_conc[i] * (w_nanodrop / 10000e-9)
		if brick == "X" :
			bricks_add_conc[i] = bricks_add_conc[i] * (X_nanodrop / 10000e-9)		
		if brick == "Y" :
			bricks_add_conc[i] = bricks_add_conc[i] * (Y_nanodrop / 10000e-9)		
		if brick == "r" :
			bricks_add_conc[i] = bricks_add_conc[i] * (r_nanodrop / 10000e-9)
		if brick == "q" :
			bricks_add_conc[i] = bricks_add_conc[i] * (q_nanodrop / 10000e-9)			

	return bricks_add_order, bricks_add_conc, bricks_wait_time






#
#
# EXAMPLE VIRTUAL EXPERIMENTS
#
#
#

def example(lane) :

	if(lane == 2) :
		bricks_add_order 		= 	['s',		'p']
		bricks_add_conc			=	[300e-9, 	300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60]

	if(lane == 3) :
		bricks_add_order 		= 	['s',		'p',		'X']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60,		30*60]

	if(lane == 4) :
		bricks_add_order 		= 	['s',		'p',		'X',		'p',		'Y']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60,		30*60,		30*60,		30*60]

	if(lane == 5) :
		bricks_add_order 		= 	['s',		'p',		'X',		'p',		'Y',		'p',		'X']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60,		30*60,		30*60,		30*60,		30*60,		30*60]

	if(lane == 6) :
		bricks_add_order 		= 	['s',		'p',		'X',		'p',		'Y',		'p',		'X',		'r',		'q']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		50e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60]

	if(lane == 7) :
		bricks_add_order 		= 	['s',		'p',		'X',		'p',		'Y',		'p',		'X',		'r',		'q',		'r',		'q']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		50e-9,		300e-9,		50e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60]

	if(lane == 8) :
		bricks_add_order 		= 	['s',		'p',		'X',		'p',		'Y',		'p',		'X',		'r',		'q',		'r',		'q',		'r',		'q']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		50e-9,		300e-9,		50e-9,		300e-9,		50e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60]

	if(lane == 9) :
		bricks_add_order	=	['s', 'p', 'X', 'p', 'Y', 'p', 'X', 'r', 'q', 'p', 'X', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'r', 'q', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'Y', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'r', 'q', 'r', 'q', 'p', 'X', 'r', 'q', 'r', 'q', 'p', 'Y', 'r', 'q']
		bricks_add_conc		=	[300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]

	return bricks_add_order, bricks_add_conc, bricks_wait_time




def example_vgel(lane) :

	if(lane == 2) :
		bricks_add_order 		= 	['s',		'p',		'X']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60,		30*60]

	if(lane == 3) :
		bricks_add_order 		= 	['s',		'p',		'X',		'p',		'X']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60,		30*60,		30*60,		30*60]

	if(lane == 4) :
		bricks_add_order 		= 	['s',		'p',		'X',		'p',		'X',		'p',		'X']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60,		30*60,		30*60,		30*60,		30*60,		30*60]

	if(lane == 5) :
		bricks_add_order 		= 	['s',		'p',		'X',		'p',		'X',		'p',		'X',		'r',		'q']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60]

	return bricks_add_order, bricks_add_conc, bricks_wait_time





#
#
# PAGE EXPERIMENTS -- STRAIGHT BRICKS
#
#


def gel43(lane) :		# 300nM bricks (only final lane is now specified, as simulator works out releaser supernatant at each step)

									#			 2           3           4           5           6           7      gel lane number
	if(lane == 7) :					#            1           2           3           4           5           6    	bricks after start
		bricks_add_order 		= 	['s',		'p',		'X',		'p',		'X',		'p',		'X']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60,		30*60,		30*60,		30*60,		30*60,		30*60]	

	return correct_concentrations(bricks_add_order, bricks_add_conc, bricks_wait_time, use_concs="May2017")


def gel33(lane) :		# 350nM bricks (only final lane is now specified, as simulator works out releaser supernatant at each step)

									#			                                     2           3           4           5 	gel lane number
	if(lane == 5) :					#            1           2           3           4           5           6           7  bricks after start
		bricks_add_order 		= 	['s',		'p',		'X',		'p',		'X',		'r',		'q',		'r']
		bricks_add_conc			=	[350e-9, 	350e-9,		350e-9,		350e-9,		350e-9,		350e-9,		350e-9,		350e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60]	

	return correct_concentrations(bricks_add_order, bricks_add_conc, bricks_wait_time, use_concs="May2017")


def gel64(lane) :		# 300nM bricks (only final lane is now specified, as simulator works out releaser supernatant at each step)

									#			 2           3                       4                       5                       6                       7           8    gel lane number
	if(lane == 8) : 				#            1           2           3           4           5           6           7           8           9           10          11   bricks after start
		bricks_add_order 		= 	['s',		'p',		'X',		'p', 		'X',		'p', 		'X',		'r',		'q',		'r',		'q',		'r']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60]

	return correct_concentrations(bricks_add_order, bricks_add_conc, bricks_wait_time, use_concs="May2017")









#
#
# BIOANALYSER EXPERIMENTS -- LOOPED BRICKS
#
#		(Note - these gels cannot have a virtual gel image made, as the bricks are looped)
#



def bioanalyserXYX(lane) :		# recording and popping 3 signals (X - Y - X)	

	bricks_add_order = []; bricks_add_conc = []; bricks_wait_time = []

									#			 2			 3						 4	  					 5 						 6 						7 						8		gel lane number
	if(lane == 8) :					#			 1			 2			 3 			 4 			 5			 6			 7 			 8 			 9	 		10 			11 			12		bricks after start
		bricks_add_order 		= 	['s',		'p',		'X',		'p',		'Y',		'p',		'X',		'r',		'q',		'r',		'q',		'r',		'q']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		50e-9,		300e-9,		50e-9,		300e-9,		50e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60,		30*60]

	return correct_concentrations(bricks_add_order, bricks_add_conc, bricks_wait_time, use_concs="Dec2018")




def bioanalyserXXY(lane) :		# recording X - X - Y, popping Y - X - X

	bricks_add_order = []; bricks_add_conc = []; bricks_wait_time = []

									#			 2			 3						 4 						 5 			 6 						 7 						8 		gel lane number
	if(lane == 8) :					#			 1			 2			 3			 4			 5 			 6			 7			 8			 9 			10 			11		bricks after start
		bricks_add_order 		= 	['s',		'p',		'X',		'p', 		'X',		'p', 		'Y',		'r',		'q',		'r',		'q',		'r']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		50e-9,		300e-9,		50e-9,		300e-9,		50e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60]	

	return correct_concentrations(bricks_add_order, bricks_add_conc, bricks_wait_time, use_concs="Dec2018")



def bioanalyserXXY_read300(lane) :

	bricks_add_order = []; bricks_add_conc = []; bricks_wait_time = []

									#			 2			 3						 4 						 5 			 6 						 7 						8 		gel lane number
	if(lane == 8) :					#			 1			 2			 3			 4			 5 			 6			 7			 8			 9 			10 			11		bricks after start
		bricks_add_order 		= 	['s',		'p',		'X',		'p', 		'X',		'p', 		'Y',		'r',		'q',		'r',		'q',		'r']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60]	

	return correct_concentrations(bricks_add_order, bricks_add_conc, bricks_wait_time, use_concs="Dec2018")




def bioanalyserYXX(lane) :		# recording Y - X - X, popping X - X - Y

	bricks_add_order = []; bricks_add_conc = []; bricks_wait_time = []

									#			 2			 3						 4						 5			 6						 7						8 		gel lane number
	if(lane == 8) : 				#			 1			 2			 3			 4			 5			 6			 7			 8 			 9			10			11		bricks after start
		bricks_add_order 		= 	['s',		'p',		'Y',		'p', 		'X',		'p', 		'X',		'r',		'q',		'r',		'q',		'r']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		50e-9,		300e-9,		50e-9,		300e-9,		50e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60]	


	return correct_concentrations(bricks_add_order, bricks_add_conc, bricks_wait_time, use_concs="Dec2018")



def bioanalyserYXX_read300(lane) :		# READ ALSO AT 300nM # recording Y - X - X, popping X - X - Y

	bricks_add_order = []; bricks_add_conc = []; bricks_wait_time = []

									#			 2			 3						 4						 5			 6						 7						8 		gel lane number
	if(lane == 8) : 				#			 1			 2			 3			 4			 5			 6			 7			 8 			 9			10			11		bricks after start
		bricks_add_order 		= 	['s',		'p',		'Y',		'p', 		'X',		'p', 		'X',		'r',		'q',		'r',		'q',		'r']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60]	


	return correct_concentrations(bricks_add_order, bricks_add_conc, bricks_wait_time, use_concs="Dec2018")





def leedsAFM_XXX(lane) :			# recording 3 signals, then AFM (Fig 3)

	bricks_add_order = []; bricks_add_conc = []; bricks_wait_time = []
	
	if(lane == 2) :
		bricks_add_order 		= 	['s',		'p',		'X',		'p', 		'X',		'p', 		'X']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60]	

	return correct_concentrations(bricks_add_order, bricks_add_conc, bricks_wait_time, use_concs="Dec2018")




def leedsAFM_XYX(lane) :

	bricks_add_order = []; bricks_add_conc = []; bricks_wait_time = []

	if(lane == 2) :
		bricks_add_order 		= 	['s',		'p',		'X',		'p', 		'Y',		'p', 		'X']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60]	

	return correct_concentrations(bricks_add_order, bricks_add_conc, bricks_wait_time, use_concs="Dec2018")




def leedsAFM_XXY(lane) :

	bricks_add_order = []; bricks_add_conc = []; bricks_wait_time = []

	if(lane == 2) :
		bricks_add_order 		= 	['s',		'p',		'X',		'p', 		'X',		'p', 		'Y']
		bricks_add_conc			=	[300e-9, 	300e-9,		300e-9,		300e-9,		300e-9,		300e-9,		300e-9]
		bricks_wait_time 		= 	[		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60, 		30*60]	

	return correct_concentrations(bricks_add_order, bricks_add_conc, bricks_wait_time, use_concs="Dec2018")








#
# LARGE SIMULATED EXPERIMENTS FOR FIGURE 4 MODEL PREDICTIONS
#	(generated by opseq.py)
#

def fig5a(lane) :

	bricks_add_order = []; bricks_add_conc = []; bricks_wait_time = []

	# 1111100000221110000022222000002111200000
	if lane == 1 :
		bricks_add_order	=	['s', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q']
		bricks_add_conc		=	[300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]
	# 2211212221000000000012211122210000000000
	if lane == 2 :
		bricks_add_order	=	['s', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q']
		bricks_add_conc		=	[300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]
	# 1210110002201211210000221200000020010020
	if lane == 3 :
		bricks_add_order	=	['s', 'p', 'X', 'p', 'Y', 'p', 'X', 'r', 'q', 'p', 'X', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'r', 'q', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'Y', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'r', 'q', 'r', 'q', 'p', 'X', 'r', 'q', 'r', 'q', 'p', 'Y', 'r', 'q']
		bricks_add_conc		=	[300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]
	# 1212121222121212121200000000000000000000
	if lane == 4 :
		bricks_add_order	=	['s', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q']
		bricks_add_conc		=	[300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]
	# 2020102010202010101020102020201020101010
	if lane == 5 :
		bricks_add_order	=	['s', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q']
		bricks_add_conc		=	[300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]

	#correct_concentrations(bricks_add_order, bricks_add_conc, bricks_wait_time, use_concs="Dec2018")
	return bricks_add_order, bricks_add_conc, bricks_wait_time


def fig5b(lane) :

	bricks_add_order = []; bricks_add_conc = []; bricks_wait_time = []

	# 1111100000221110000022222000002111200000
	if lane == 1 :
		bricks_add_order	=	['s', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q']
		bricks_add_conc		=	[300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]
	# 2211212221000000000012211122210000000000
	if lane == 2 :
		bricks_add_order	=	['s', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q']
		bricks_add_conc		=	[300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]
	# 1210110002201211210000221200000020010020
	if lane == 3 :
		bricks_add_order	=	['s', 'p', 'X', 'p', 'Y', 'p', 'X', 'r', 'q', 'p', 'X', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'r', 'q', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'Y', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'r', 'q', 'r', 'q', 'p', 'X', 'r', 'q', 'r', 'q', 'p', 'Y', 'r', 'q']
		bricks_add_conc		=	[300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]
	# 1212121222121212121200000000000000000000
	if lane == 4 :
		bricks_add_order	=	['s', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q']
		bricks_add_conc		=	[300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]
	# 2020102010202010101020102020201020101010
	if lane == 5 :
		bricks_add_order	=	['s', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q']
		bricks_add_conc		=	[300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9, 300e-9]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]

	return bricks_add_order, bricks_add_conc, bricks_wait_time



def fig5c(lane, spXY=300e-9, rq=300e-9, wt=30*60) :

	bricks_add_order = []; bricks_add_conc = []; bricks_wait_time = []

	# 1111100000221110000022222000002111200000
	if lane == 1 :
		bricks_add_order	=	['s', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q']
		bricks_add_conc		=	[spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq]
		bricks_wait_time	=	[30*60, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt]
	# 2211212221000000000012211122210000000000
	if lane == 2 :
		bricks_add_order	=	['s', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q']
		bricks_add_conc		=	[spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq]
		bricks_wait_time	=	[30*60, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt]
	# 1210110002201211210000221200000020010020
	if lane == 3 :
		bricks_add_order	=	['s', 'p', 'X', 'p', 'Y', 'p', 'X', 'r', 'q', 'p', 'X', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'r', 'q', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'Y', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'r', 'q', 'r', 'q', 'p', 'X', 'r', 'q', 'r', 'q', 'p', 'Y', 'r', 'q']
		bricks_add_conc		=	[spXY, spXY, spXY, spXY, spXY, spXY, spXY, rq, rq, spXY, spXY, spXY, spXY, rq, rq, rq, rq, rq, rq, spXY, spXY, spXY, spXY, rq, rq, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, rq, rq, rq, rq, rq, rq, rq, rq, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, spXY, spXY, rq, rq, rq, rq, spXY, spXY, rq, rq, rq, rq, spXY, spXY, rq, rq]
		bricks_wait_time	=	[30*60, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt]
	# 1212121222121212121200000000000000000000
	if lane == 4 :
		bricks_add_order	=	['s', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q']
		bricks_add_conc		=	[spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, spXY, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq, rq]
		bricks_wait_time	=	[30*60, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt]
	# 2020102010202010101020102020201020101010
	if lane == 5 :
		bricks_add_order	=	['s', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q']
		bricks_add_conc		=	[spXY, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq, spXY, spXY, rq, rq]
		bricks_wait_time	=	[30*60, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt, wt]

	return bricks_add_order, bricks_add_conc, bricks_wait_time



def fig5d(lane, eta=100e-9) :

	bricks_add_order = []; bricks_add_conc = []; bricks_wait_time = []

	r0 = rand(410)		# array of 410 random concs, to add to brick concs as noise
	r = [(-1 + (v * 2)) * eta for v in r0]		# scale to random numbers between -1 and +1, then to random numbers between -eta and +eta

	# 1111100000221110000022222000002111200000
	if lane == 1 :
		bricks_add_order	=	['s', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q']
		bricks_add_conc		=	[300e-9+r[0], 300e-9+r[1], 300e-9+r[2], 300e-9+r[3], 300e-9+r[4], 300e-9+r[5], 300e-9+r[6], 300e-9+r[7], 300e-9+r[8], 300e-9+r[9], 300e-9+r[10], 300e-9+r[11], 300e-9+r[12], 300e-9+r[13], 300e-9+r[14], 300e-9+r[15], 300e-9+r[16], 300e-9+r[17], 300e-9+r[18], 300e-9+r[19], 300e-9+r[20], 300e-9+r[21], 300e-9+r[22], 300e-9+r[23], 300e-9+r[24], 300e-9+r[25], 300e-9+r[26], 300e-9+r[27], 300e-9+r[28], 300e-9+r[29], 300e-9+r[30], 300e-9+r[31], 300e-9+r[32], 300e-9+r[33], 300e-9+r[34], 300e-9+r[35], 300e-9+r[36], 300e-9+r[37], 300e-9+r[38], 300e-9+r[39], 300e-9+r[40], 300e-9+r[41], 300e-9+r[42], 300e-9+r[43], 300e-9+r[44], 300e-9+r[45], 300e-9+r[46], 300e-9+r[47], 300e-9+r[48], 300e-9+r[49], 300e-9+r[50], 300e-9+r[51], 300e-9+r[52], 300e-9+r[53], 300e-9+r[54], 300e-9+r[55], 300e-9+r[56], 300e-9+r[57], 300e-9+r[58], 300e-9+r[59], 300e-9+r[60], 300e-9+r[61], 300e-9+r[62], 300e-9+r[63], 300e-9+r[64], 300e-9+r[65], 300e-9+r[66], 300e-9+r[67], 300e-9+r[68], 300e-9+r[69], 300e-9+r[70], 300e-9+r[71], 300e-9+r[72], 300e-9+r[73], 300e-9+r[74], 300e-9+r[75], 300e-9+r[76], 300e-9+r[77], 300e-9+r[78], 300e-9+r[79], 300e-9+r[80]]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]
	# 2211212221000000000012211122210000000000
	if lane == 2 :
		bricks_add_order	=	['s', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q']
		bricks_add_conc		=	[300e-9+r[81], 300e-9+r[82], 300e-9+r[83], 300e-9+r[84], 300e-9+r[85], 300e-9+r[86], 300e-9+r[87], 300e-9+r[88], 300e-9+r[89], 300e-9+r[90], 300e-9+r[91], 300e-9+r[92], 300e-9+r[93], 300e-9+r[94], 300e-9+r[95], 300e-9+r[96], 300e-9+r[97], 300e-9+r[98], 300e-9+r[99], 300e-9+r[100], 300e-9+r[101], 300e-9+r[102], 300e-9+r[103], 300e-9+r[104], 300e-9+r[105], 300e-9+r[106], 300e-9+r[107], 300e-9+r[108], 300e-9+r[109], 300e-9+r[110], 300e-9+r[111], 300e-9+r[112], 300e-9+r[113], 300e-9+r[114], 300e-9+r[115], 300e-9+r[116], 300e-9+r[117], 300e-9+r[118], 300e-9+r[119], 300e-9+r[120], 300e-9+r[121], 300e-9+r[122], 300e-9+r[123], 300e-9+r[124], 300e-9+r[125], 300e-9+r[126], 300e-9+r[127], 300e-9+r[128], 300e-9+r[129], 300e-9+r[130], 300e-9+r[131], 300e-9+r[132], 300e-9+r[133], 300e-9+r[134], 300e-9+r[135], 300e-9+r[136], 300e-9+r[137], 300e-9+r[138], 300e-9+r[139], 300e-9+r[140], 300e-9+r[141], 300e-9+r[142], 300e-9+r[143], 300e-9+r[144], 300e-9+r[145], 300e-9+r[146], 300e-9+r[147], 300e-9+r[148], 300e-9+r[149], 300e-9+r[150], 300e-9+r[151], 300e-9+r[152], 300e-9+r[153], 300e-9+r[154], 300e-9+r[155], 300e-9+r[156], 300e-9+r[157], 300e-9+r[158], 300e-9+r[159], 300e-9+r[160], 300e-9+r[161]]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]
	# 1210110002201211210000221200000020010020
	if lane == 3 :
		bricks_add_order	=	['s', 'p', 'X', 'p', 'Y', 'p', 'X', 'r', 'q', 'p', 'X', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'r', 'q', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'X', 'p', 'Y', 'p', 'X', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'p', 'Y', 'r', 'q', 'r', 'q', 'p', 'X', 'r', 'q', 'r', 'q', 'p', 'Y', 'r', 'q']
		bricks_add_conc		=	[300e-9+r[162], 300e-9+r[163], 300e-9+r[164], 300e-9+r[165], 300e-9+r[166], 300e-9+r[167], 300e-9+r[168], 300e-9+r[169], 300e-9+r[170], 300e-9+r[171], 300e-9+r[172], 300e-9+r[173], 300e-9+r[174], 300e-9+r[175], 300e-9+r[176], 300e-9+r[177], 300e-9+r[178], 300e-9+r[179], 300e-9+r[180], 300e-9+r[181], 300e-9+r[182], 300e-9+r[183], 300e-9+r[184], 300e-9+r[185], 300e-9+r[186], 300e-9+r[187], 300e-9+r[188], 300e-9+r[189], 300e-9+r[190], 300e-9+r[191], 300e-9+r[192], 300e-9+r[193], 300e-9+r[194], 300e-9+r[195], 300e-9+r[196], 300e-9+r[197], 300e-9+r[198], 300e-9+r[199], 300e-9+r[200], 300e-9+r[201], 300e-9+r[202], 300e-9+r[203], 300e-9+r[204], 300e-9+r[205], 300e-9+r[206], 300e-9+r[207], 300e-9+r[208], 300e-9+r[209], 300e-9+r[210], 300e-9+r[211], 300e-9+r[212], 300e-9+r[213], 300e-9+r[214], 300e-9+r[215], 300e-9+r[216], 300e-9+r[217], 300e-9+r[218], 300e-9+r[219], 300e-9+r[220], 300e-9+r[221], 300e-9+r[222], 300e-9+r[223], 300e-9+r[224], 300e-9+r[225], 300e-9+r[226], 300e-9+r[227], 300e-9+r[228], 300e-9+r[229], 300e-9+r[230], 300e-9+r[231], 300e-9+r[232], 300e-9+r[233], 300e-9+r[234], 300e-9+r[235], 300e-9+r[236], 300e-9+r[237], 300e-9+r[238], 300e-9+r[239], 300e-9+r[240], 300e-9+r[241], 300e-9+r[242]]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]
	# 1212121222121212121200000000000000000000
	if lane == 4 :
		bricks_add_order	=	['s', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'Y', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'p', 'X', 'p', 'Y', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q', 'r', 'q']
		bricks_add_conc		=	[300e-9+r[243], 300e-9+r[244], 300e-9+r[245], 300e-9+r[246], 300e-9+r[247], 300e-9+r[248], 300e-9+r[249], 300e-9+r[250], 300e-9+r[251], 300e-9+r[252], 300e-9+r[253], 300e-9+r[254], 300e-9+r[255], 300e-9+r[256], 300e-9+r[257], 300e-9+r[258], 300e-9+r[259], 300e-9+r[260], 300e-9+r[261], 300e-9+r[262], 300e-9+r[263], 300e-9+r[264], 300e-9+r[265], 300e-9+r[266], 300e-9+r[267], 300e-9+r[268], 300e-9+r[269], 300e-9+r[270], 300e-9+r[271], 300e-9+r[272], 300e-9+r[273], 300e-9+r[274], 300e-9+r[275], 300e-9+r[276], 300e-9+r[277], 300e-9+r[278], 300e-9+r[279], 300e-9+r[280], 300e-9+r[281], 300e-9+r[282], 300e-9+r[283], 300e-9+r[284], 300e-9+r[285], 300e-9+r[286], 300e-9+r[287], 300e-9+r[288], 300e-9+r[289], 300e-9+r[290], 300e-9+r[291], 300e-9+r[292], 300e-9+r[293], 300e-9+r[294], 300e-9+r[295], 300e-9+r[296], 300e-9+r[297], 300e-9+r[298], 300e-9+r[299], 300e-9+r[300], 300e-9+r[301], 300e-9+r[302], 300e-9+r[303], 300e-9+r[304], 300e-9+r[305], 300e-9+r[306], 300e-9+r[307], 300e-9+r[308], 300e-9+r[309], 300e-9+r[310], 300e-9+r[311], 300e-9+r[312], 300e-9+r[313], 300e-9+r[314], 300e-9+r[315], 300e-9+r[316], 300e-9+r[317], 300e-9+r[318], 300e-9+r[319], 300e-9+r[320], 300e-9+r[321], 300e-9+r[322], 300e-9+r[323]]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]
	# 2020102010202010101020102020201020101010
	if lane == 5 :
		bricks_add_order	=	['s', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'Y', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q', 'p', 'X', 'r', 'q']
		bricks_add_conc		=	[300e-9+r[324], 300e-9+r[325], 300e-9+r[326], 300e-9+r[327], 300e-9+r[328], 300e-9+r[329], 300e-9+r[330], 300e-9+r[331], 300e-9+r[332], 300e-9+r[333], 300e-9+r[334], 300e-9+r[335], 300e-9+r[336], 300e-9+r[337], 300e-9+r[338], 300e-9+r[339], 300e-9+r[340], 300e-9+r[341], 300e-9+r[342], 300e-9+r[343], 300e-9+r[344], 300e-9+r[345], 300e-9+r[346], 300e-9+r[347], 300e-9+r[348], 300e-9+r[349], 300e-9+r[350], 300e-9+r[351], 300e-9+r[352], 300e-9+r[353], 300e-9+r[354], 300e-9+r[355], 300e-9+r[356], 300e-9+r[357], 300e-9+r[358], 300e-9+r[359], 300e-9+r[360], 300e-9+r[361], 300e-9+r[362], 300e-9+r[363], 300e-9+r[364], 300e-9+r[365], 300e-9+r[366], 300e-9+r[367], 300e-9+r[368], 300e-9+r[369], 300e-9+r[370], 300e-9+r[371], 300e-9+r[372], 300e-9+r[373], 300e-9+r[374], 300e-9+r[375], 300e-9+r[376], 300e-9+r[377], 300e-9+r[378], 300e-9+r[379], 300e-9+r[380], 300e-9+r[381], 300e-9+r[382], 300e-9+r[383], 300e-9+r[384], 300e-9+r[385], 300e-9+r[386], 300e-9+r[387], 300e-9+r[388], 300e-9+r[389], 300e-9+r[390], 300e-9+r[391], 300e-9+r[392], 300e-9+r[393], 300e-9+r[394], 300e-9+r[395], 300e-9+r[396], 300e-9+r[397], 300e-9+r[398], 300e-9+r[399], 300e-9+r[400], 300e-9+r[401], 300e-9+r[402], 300e-9+r[403], 300e-9+r[404]]
		bricks_wait_time	=	[30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60, 30*60]

	return bricks_add_order, bricks_add_conc, bricks_wait_time



lane1_opseq = "1111100000221110000022222000002111200000"  #seq5
lane2_opseq = "2211212221000000000012211122210000000000"  #seq10
lane3_opseq = "1210110002201211210000221200000020010020"  #seqR (random) *
lane4_opseq = "1212121222121212121200000000000000000000"  #seq20 *
lane5_opseq = "2020102010202010101020102020201020101010"  #seq1 *




lane1_target = {}
lane1_target[2] = "spX"
lane1_target[4] = "spXpX"
lane1_target[6] = "spXpXpX"
lane1_target[8] = "spXpXpXpX"
lane1_target[10] = "spXpXpXpXpX"
lane1_target[11] = "spXpXpXpXp"
lane1_target[12] = "spXpXpXpX"
lane1_target[13] = "spXpXpXp"
lane1_target[14] = "spXpXpX"
lane1_target[15] = "spXpXp"
lane1_target[16] = "spXpX"
lane1_target[17] = "spXp"
lane1_target[18] = "spX"
lane1_target[19] = "sp"
lane1_target[20] = "s"
lane1_target[22] = "spY"
lane1_target[24] = "spYpY"
lane1_target[26] = "spYpYpX"
lane1_target[28] = "spYpYpXpX"
lane1_target[30] = "spYpYpXpXpX"
lane1_target[31] = "spYpYpXpXp"
lane1_target[32] = "spYpYpXpX"
lane1_target[33] = "spYpYpXp"
lane1_target[34] = "spYpYpX"
lane1_target[35] = "spYpYp"
lane1_target[36] = "spYpY"
lane1_target[37] = "spYp"
lane1_target[38] = "spY"
lane1_target[39] = "sp"
lane1_target[40] = "s"
lane1_target[42] = "spY"
lane1_target[44] = "spYpY"
lane1_target[46] = "spYpYpY"
lane1_target[48] = "spYpYpYpY"
lane1_target[50] = "spYpYpYpYpY"
lane1_target[51] = "spYpYpYpYp"
lane1_target[52] = "spYpYpYpY"
lane1_target[53] = "spYpYpYp"
lane1_target[54] = "spYpYpY"
lane1_target[55] = "spYpYp"
lane1_target[56] = "spYpY"
lane1_target[57] = "spYp"
lane1_target[58] = "spY"
lane1_target[59] = "sp"
lane1_target[60] = "s"
lane1_target[62] = "spY"
lane1_target[64] = "spYpX"
lane1_target[66] = "spYpXpX"
lane1_target[68] = "spYpXpXpX"
lane1_target[70] = "spYpXpXpXpY"
lane1_target[71] = "spYpXpXpXp"
lane1_target[72] = "spYpXpXpX"
lane1_target[73] = "spYpXpXp"
lane1_target[74] = "spYpXpX"
lane1_target[75] = "spYpXp"
lane1_target[76] = "spYpX"
lane1_target[77] = "spYp"
lane1_target[78] = "spY"
lane1_target[79] = "sp"
lane1_target[80] = "s"


lane2_target = {}
lane2_target[2] = "spY"
lane2_target[4] = "spYpY"
lane2_target[6] = "spYpYpX"
lane2_target[8] = "spYpYpXpX"
lane2_target[10] = "spYpYpXpXpY"
lane2_target[12] = "spYpYpXpXpYpX"
lane2_target[14] = "spYpYpXpXpYpXpY"
lane2_target[16] = "spYpYpXpXpYpXpYpY"
lane2_target[18] = "spYpYpXpXpYpXpYpYpY"
lane2_target[20] = "spYpYpXpXpYpXpYpYpYpX"
lane2_target[21] = "spYpYpXpXpYpXpYpYpYp"
lane2_target[22] = "spYpYpXpXpYpXpYpYpY"
lane2_target[23] = "spYpYpXpXpYpXpYpYp"
lane2_target[24] = "spYpYpXpXpYpXpYpY"
lane2_target[25] = "spYpYpXpXpYpXpYp"
lane2_target[26] = "spYpYpXpXpYpXpY"
lane2_target[27] = "spYpYpXpXpYpXp"
lane2_target[28] = "spYpYpXpXpYpX"
lane2_target[29] = "spYpYpXpXpYp"
lane2_target[30] = "spYpYpXpXpY"
lane2_target[31] = "spYpYpXpXp"
lane2_target[32] = "spYpYpXpX"
lane2_target[33] = "spYpYpXp"
lane2_target[34] = "spYpYpX"
lane2_target[35] = "spYpYp"
lane2_target[36] = "spYpY"
lane2_target[37] = "spYp"
lane2_target[38] = "spY"
lane2_target[39] = "sp"
lane2_target[40] = "s"
lane2_target[42] = "spX"
lane2_target[44] = "spXpY"
lane2_target[46] = "spXpYpY"
lane2_target[48] = "spXpYpYpX"
lane2_target[50] = "spXpYpYpXpX"
lane2_target[52] = "spXpYpYpXpXpX"
lane2_target[54] = "spXpYpYpXpXpXpY"
lane2_target[56] = "spXpYpYpXpXpXpYpY"
lane2_target[58] = "spXpYpYpXpXpXpYpYpY"
lane2_target[60] = "spXpYpYpXpXpXpYpYpYpX"
lane2_target[61] = "spXpYpYpXpXpXpYpYpYp"
lane2_target[62] = "spXpYpYpXpXpXpYpYpY"
lane2_target[63] = "spXpYpYpXpXpXpYpYp"
lane2_target[64] = "spXpYpYpXpXpXpYpY"
lane2_target[65] = "spXpYpYpXpXpXpYp"
lane2_target[66] = "spXpYpYpXpXpXpY"
lane2_target[67] = "spXpYpYpXpXpXp"
lane2_target[68] = "spXpYpYpXpXpX"
lane2_target[69] = "spXpYpYpXpXp"
lane2_target[70] = "spXpYpYpXpX"
lane2_target[71] = "spXpYpYpXp"
lane2_target[72] = "spXpYpYpX"
lane2_target[73] = "spXpYpYp"
lane2_target[74] = "spXpYpY"
lane2_target[75] = "spXpYp"
lane2_target[76] = "spXpY"
lane2_target[77] = "spXp"
lane2_target[78] = "spX"
lane2_target[79] = "sp"
lane2_target[80] = "s"


lane3_target = {}
lane3_target[2] = "spX"
lane3_target[4] = "spXpY"
lane3_target[6] = "spXpYpX"
lane3_target[7] = "spXpYp"
lane3_target[8] = "spXpY"
lane3_target[10] = "spXpYpX"
lane3_target[12] = "spXpYpXpX"
lane3_target[13] = "spXpYpXp"
lane3_target[14] = "spXpYpX"
lane3_target[15] = "spXpYp"
lane3_target[16] = "spXpY"
lane3_target[17] = "spXp"
lane3_target[18] = "spX"
lane3_target[20] = "spXpY"
lane3_target[22] = "spXpYpY"
lane3_target[23] = "spXpYp"
lane3_target[24] = "spXpY"
lane3_target[26] = "spXpYpX"
lane3_target[28] = "spXpYpXpY"
lane3_target[30] = "spXpYpXpYpX"
lane3_target[32] = "spXpYpXpYpXpX"
lane3_target[34] = "spXpYpXpYpXpXpY"
lane3_target[36] = "spXpYpXpYpXpXpYpX"
lane3_target[37] = "spXpYpXpYpXpXpYp"
lane3_target[38] = "spXpYpXpYpXpXpY"
lane3_target[39] = "spXpYpXpYpXpXp"
lane3_target[40] = "spXpYpXpYpXpX"
lane3_target[41] = "spXpYpXpYpXp"
lane3_target[42] = "spXpYpXpYpX"
lane3_target[43] = "spXpYpXpYp"
lane3_target[44] = "spXpYpXpY"
lane3_target[46] = "spXpYpXpYpY"
lane3_target[48] = "spXpYpXpYpYpY"
lane3_target[50] = "spXpYpXpYpYpYpX"
lane3_target[52] = "spXpYpXpYpYpYpXpY"
lane3_target[53] = "spXpYpXpYpYpYpXp"
lane3_target[54] = "spXpYpXpYpYpYpX"
lane3_target[55] = "spXpYpXpYpYpYp"
lane3_target[56] = "spXpYpXpYpYpY"
lane3_target[57] = "spXpYpXpYpYp"
lane3_target[58] = "spXpYpXpYpY"
lane3_target[59] = "spXpYpXpYp"
lane3_target[60] = "spXpYpXpY"
lane3_target[61] = "spXpYpXp"
lane3_target[62] = "spXpYpX"
lane3_target[63] = "spXpYp"
lane3_target[64] = "spXpY"
lane3_target[66] = "spXpYpY"
lane3_target[67] = "spXpYp"
lane3_target[68] = "spXpY"
lane3_target[69] = "spXp"
lane3_target[70] = "spX"
lane3_target[72] = "spXpX"
lane3_target[73] = "spXp"
lane3_target[74] = "spX"
lane3_target[75] = "sp"
lane3_target[76] = "s"
lane3_target[78] = "spY"
lane3_target[79] = "sp"
lane3_target[80] = "s"


lane4_target = {}
lane4_target[2] = "spX"
lane4_target[4] = "spXpY"
lane4_target[6] = "spXpYpX"
lane4_target[8] = "spXpYpXpY"
lane4_target[10] = "spXpYpXpYpX"
lane4_target[12] = "spXpYpXpYpXpY"
lane4_target[14] = "spXpYpXpYpXpYpX"
lane4_target[16] = "spXpYpXpYpXpYpXpY"
lane4_target[18] = "spXpYpXpYpXpYpXpYpY"
lane4_target[20] = "spXpYpXpYpXpYpXpYpYpY"
lane4_target[22] = "spXpYpXpYpXpYpXpYpYpYpX"
lane4_target[24] = "spXpYpXpYpXpYpXpYpYpYpXpY"
lane4_target[26] = "spXpYpXpYpXpYpXpYpYpYpXpYpX"
lane4_target[28] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpY"
lane4_target[30] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpX"
lane4_target[32] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpXpY"
lane4_target[34] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpXpYpX"
lane4_target[36] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpXpYpXpY"
lane4_target[38] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpXpYpXpYpX"
lane4_target[40] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpXpYpXpYpXpY"
lane4_target[41] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpXpYpXpYpXp"
lane4_target[42] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpXpYpXpYpX"
lane4_target[43] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpXpYpXpYp"
lane4_target[44] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpXpYpXpY"
lane4_target[45] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpXpYpXp"
lane4_target[46] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpXpYpX"
lane4_target[47] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpXpYp"
lane4_target[48] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpXpY"
lane4_target[49] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpXp"
lane4_target[50] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYpX"
lane4_target[51] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpYp"
lane4_target[52] = "spXpYpXpYpXpYpXpYpYpYpXpYpXpY"
lane4_target[53] = "spXpYpXpYpXpYpXpYpYpYpXpYpXp"
lane4_target[54] = "spXpYpXpYpXpYpXpYpYpYpXpYpX"
lane4_target[55] = "spXpYpXpYpXpYpXpYpYpYpXpYp"
lane4_target[56] = "spXpYpXpYpXpYpXpYpYpYpXpY"
lane4_target[57] = "spXpYpXpYpXpYpXpYpYpYpXp"
lane4_target[58] = "spXpYpXpYpXpYpXpYpYpYpX"
lane4_target[59] = "spXpYpXpYpXpYpXpYpYpYp"
lane4_target[60] = "spXpYpXpYpXpYpXpYpYpY"
lane4_target[61] = "spXpYpXpYpXpYpXpYpYp"
lane4_target[62] = "spXpYpXpYpXpYpXpYpY"
lane4_target[63] = "spXpYpXpYpXpYpXpYp"
lane4_target[64] = "spXpYpXpYpXpYpXpY"
lane4_target[65] = "spXpYpXpYpXpYpXp"
lane4_target[66] = "spXpYpXpYpXpYpX"
lane4_target[67] = "spXpYpXpYpXpYp"
lane4_target[68] = "spXpYpXpYpXpY"
lane4_target[69] = "spXpYpXpYpXp"
lane4_target[70] = "spXpYpXpYpX"
lane4_target[71] = "spXpYpXpYp"
lane4_target[72] = "spXpYpXpY"
lane4_target[73] = "spXpYpXp"
lane4_target[74] = "spXpYpX"
lane4_target[75] = "spXpYp"
lane4_target[76] = "spXpY"
lane4_target[77] = "spXp"
lane4_target[78] = "spX"
lane4_target[79] = "sp"
lane4_target[80] = "s"


lane5_target = {}
lane5_target[2] = "spY"
lane5_target[3] = "sp"
lane5_target[4] = "s"
lane5_target[6] = "spY"
lane5_target[7] = "sp"
lane5_target[8] = "s"
lane5_target[10] = "spX"
lane5_target[11] = "sp"
lane5_target[12] = "s"
lane5_target[14] = "spY"
lane5_target[15] = "sp"
lane5_target[16] = "s"
lane5_target[18] = "spX"
lane5_target[19] = "sp"
lane5_target[20] = "s"
lane5_target[22] = "spY"
lane5_target[23] = "sp"
lane5_target[24] = "s"
lane5_target[26] = "spY"
lane5_target[27] = "sp"
lane5_target[28] = "s"
lane5_target[30] = "spX"
lane5_target[31] = "sp"
lane5_target[32] = "s"
lane5_target[34] = "spX"
lane5_target[35] = "sp"
lane5_target[36] = "s"
lane5_target[38] = "spX"
lane5_target[39] = "sp"
lane5_target[40] = "s"
lane5_target[42] = "spY"
lane5_target[43] = "sp"
lane5_target[44] = "s"
lane5_target[46] = "spX"
lane5_target[47] = "sp"
lane5_target[48] = "s"
lane5_target[50] = "spY"
lane5_target[51] = "sp"
lane5_target[52] = "s"
lane5_target[54] = "spY"
lane5_target[55] = "sp"
lane5_target[56] = "s"
lane5_target[58] = "spY"
lane5_target[59] = "sp"
lane5_target[60] = "s"
lane5_target[62] = "spX"
lane5_target[63] = "sp"
lane5_target[64] = "s"
lane5_target[66] = "spY"
lane5_target[67] = "sp"
lane5_target[68] = "s"
lane5_target[70] = "spX"
lane5_target[71] = "sp"
lane5_target[72] = "s"
lane5_target[74] = "spX"
lane5_target[75] = "sp"
lane5_target[76] = "s"
lane5_target[78] = "spX"
lane5_target[79] = "sp"
lane5_target[80] = "s"