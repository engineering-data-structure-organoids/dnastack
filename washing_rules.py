"""20 STOCAL rules for washing model

The rules work with X and Y signals

To simulate the linear bricks system where w exists instead of X or Y
I replace all w with X, so it can still be simulated

Fraying and circularisation not included.

Ben Shirt-Ediss, May/June 2019
"""

import constants
from stocal import *


# S + (p)stack --> (Sp)stack  
#	(where S is ks or s)
class LH1(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if (k == "s" or k == "ks") and l.startswith("p") :
			yield self.Transition([k,l], [k+l], constants.C_KA)
		elif (l == "s" or l == "ks") and k.startswith("p") :
			yield self.Transition([k,l], [l+k], constants.C_KA)


# stacki(Z) + (p)stackj --> stacki(Zp)stackj
#	(where Z is X or Y)
# XXX CAN BE A DIMERISATION
class LH2(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :	
		if (k.endswith("X") or k.endswith("Y")) and l.startswith("p") :
			yield self.Transition([k,l], [k+l], constants.C_KA)
		elif (l.endswith("X") or l.endswith("Y")) and k.startswith("p") :
			yield self.Transition([k,l], [l+k], constants.C_KA)


# stacki(p) + (Z)stackj --> stacki(pZ)stackj
#	(where Z is X or Y)
# XXX CAN BE A DIMERISATION
class LH3(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if k.endswith("p") and (l.startswith("X") or l.startswith("Y")) :
			yield self.Transition([k,l], [k+l], constants.C_KBC)
		elif l.endswith("p") and (k.startswith("X") or k.startswith("Y")) :
			yield self.Transition([k,l], [l+k], constants.C_KBC)


# Z + r --> IZrI
#	(where Z is X or Y)
class H1(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if (k == "X" or k == "Y") and l == "r" :
			yield self.Transition([k,l], ["I"+k+l+"I"], constants.C_KABC)
		elif (l == "X" or l == "Y") and k == "r" :
			yield self.Transition([k,l], ["I"+l+k+"I"], constants.C_KABC)


# p + q --> IpqI
class H2(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if k == "p" and l == "q" :
			yield self.Transition([k,l], ["I"+k+l+"I"], constants.C_KABC)
		elif l == "p" and k == "q" :
			yield self.Transition([k,l], ["I"+l+k+"I"], constants.C_KABC)


# tau(r) + S --> tau(rS)

# note, reactions for attaching start
# qrqr + s --> qrqrs
# note, reaction for attaching tethered start
# qrqr + ks --> qrqrks	
#	but here, r does not bind to k, it binds to s. Both r and k come off s in the same direction.

class LH4(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if k.endswith("r") and (l == "s" or l == "ks") :
			yield self.Transition([k,l], [k+l], constants.C_KA)
		elif l.endswith("r") and (k == "s" or k == "ks") :
			yield self.Transition([k,l], [l+k], constants.C_KA)


# tau_i(r) + (q)tau_j --> tau_i(rq)tau_j
# XXX CAN BE A DIMERISATION
class LH5(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if k.endswith("r") and l.startswith("q") :
			yield self.Transition([k,l], [k+l], constants.C_KA)
		elif l.endswith("r") and k.startswith("q") :			
			yield self.Transition([k,l], [l+k], constants.C_KA)


# tau_i(q) + (r)tau_j --> tau_i(qr)tau_j
# XXX CAN BE A DIMERISATION
class LH6(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if k.endswith("q") and l.startswith("r") :
			yield self.Transition([k,l], [k+l], constants.C_KBC)
		elif l.endswith("q") and k.startswith("r") :
			yield self.Transition([k,l], [l+k], constants.C_KBC)


# sigma(pZ) + r --> sigma(p) + IZrI
class SD1(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if (k.endswith("X") or k.endswith("Y")) and len(k) >= 2 and l == "r" :
			product1 = k[:-1]
			product2 = "I" + k[-1] + "rI"
			yield self.Transition([k,l], [product1, product2], constants.C_K1)
		elif (l.endswith("X") or l.endswith("Y")) and len(l) >= 2 and k == "r" :
			product1 = l[:-1]
			product2 = "I" + l[-1] + "rI"
			yield self.Transition([k,l], [product1, product2], constants.C_K1)


# sigma((Z or S)p) + q --> sigma(Z or S) + IpqI
class SD2(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if k.endswith("p") and len(k) >= 2 and l == "q" :
			product1 = k[:-1]
			product2 = "IpqI"
			yield self.Transition([k,l], [product1, product2], constants.C_K2)
		elif l.endswith("p") and len(l) >= 2 and k == "q" :
			product1 = l[:-1]
			product2 = "IpqI"
			yield self.Transition([k,l], [product1, product2], constants.C_K2)			



# r + (Zp)sigma --> IZrI + (p)sigma
class SD3(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if k == "r" and (l.startswith("X") or l.startswith("Y")) and len(l) >= 2 :
			product1 = "I" + l[0] + "rI"
			product2 = l[1:]
			yield self.Transition([k,l], [product1, product2], constants.C_K2)
		elif l == "r" and (k.startswith("X") or k.startswith("Y")) and len(k) >= 2 :
			product1 = "I" + k[0] + "rI"
			product2 = k[1:]
			yield self.Transition([k,l], [product1, product2], constants.C_K2)



# q + (pZ)sigma --> IpqI + (Z)sigma
class SD4(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if k == "q" and l.startswith("p") and len(l) >= 2 :
			product1 = "IpqI"
			product2 = l[1:]
			yield self.Transition([k,l], [product1, product2], constants.C_K1)
		elif l == "q" and k.startswith("p") and len(k) >= 2 :
			product1 = "IpqI"
			product2 = k[1:]
			yield self.Transition([k,l], [product1, product2], constants.C_K1)



# Z + (r(q or S))tau --> IZrI + (q or S)tau
class SD5(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if (k == "X" or k == "Y") and l.startswith("r") and len(l) >= 2 :
			product1 = "I" + k + "rI"
			product2 = l[1:]
			yield self.Transition([k,l], [product1, product2], constants.C_K2)
		elif (l == "X" or l == "Y") and k.startswith("r") and len(k) >= 2 :
			product1 = "I" + l + "rI"			
			product2 = k[1:]
			yield self.Transition([k,l], [product1, product2], constants.C_K2)



# p + (qr)tau --> IpqI + (r)tau
class SD6(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if k == "p" and l.startswith("q") and len(l) >= 2 :
			product1 = "IpqI"
			product2 = l[1:]
			yield self.Transition([k,l], [product1, product2], constants.C_K1)
		elif l == "p" and k.startswith("q") and len(k) >= 2 :
			product1 = "IpqI"
			product2 = k[1:]
			yield self.Transition([k,l], [product1, product2], constants.C_K1)



# tau(qr) + Z --> tau(q) + IZrI
class SD7(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if k.endswith("r") and len(k) >=2 and (l == "X" or l == "Y") :
			product1 = k[:-1]
			product2 = "I" + l + "rI"
			yield self.Transition([k,l], [product1, product2], constants.C_K1)
		elif l.endswith("r") and len(l) >=2 and (k == "X" or k == "Y") :
			product1 = l[:-1]
			product2 = "I" + k + "rI"
			yield self.Transition([k,l], [product1, product2], constants.C_K1)



# tau(rq) + p --> tau(r) + IpqI
class SD8(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :
		if k.endswith("q") and len(k) >= 2 and l == "p" :
			product1 = k[:-1]
			product2 = "IpqI"
			yield self.Transition([k,l], [product1, product2], constants.C_K2)
		elif l.endswith("q") and len(l) >= 2 and k == "p" :
			product1 = l[:-1]
			product2 = "IpqI"
			yield self.Transition([k,l], [product1, product2], constants.C_K2)







#
# Chain annihilation rules
#


class ChainOutcome() :
	# (this is not a rule!)
	# these static methods work out the final products of a chain strand displacement

	@staticmethod 
	def right_align(top, bottom) :

		# -- Adaptation for washing chemistry with linkers --
		# linker-start behaves the same as start
		# so, ks is replaced with s here, and at the end, s is replaced with ks, to keep the alignment working
		ks_replaced_by_s = False
		if ("ks" in top) or ("ks" in bottom) :
			top = top.replace("ks", "s")
			bottom = bottom.replace("ks", "s")
			ks_replaced_by_s = True
		# ---------------------------------------------------

		# for rules C1 and C2
		# right-align top and bottom (strand displacement from end)
		t = len(top); b = len(bottom)
		if t > b :
			bottom = "-"*(t-b) + bottom
		elif b > t :
			top = "-"*(b-t) + top

		# scan from right, making pairs
		Xr_num = 0
		Yr_num = 0
		pq_num = 0; rs_num = 0

		for i in range(len(top)-1, -1, -1) :
			if top[i] == "X" and bottom[i] == "r" :
				Xr_num += 1
			elif top[i] == "Y" and bottom[i] == "r" :
				Yr_num += 1
			elif top[i] == "p" and bottom[i] == "q" :
				pq_num += 1
			elif top[i] == "s" and bottom[i] == "r" :
				rs_num += 1

		residual = ""
		if t > b :
			residual = top[:(t-b)]
		elif b > t :
			residual = bottom[:(b-t)]
			# which can also end in an rs pair
			if rs_num > 0 :
				residual += "rs"
		elif b == t :
			if rs_num > 0 :
				residual = "rs"

		# -- Adaptation for washing chemistry with linkers --
		if ks_replaced_by_s :
			residual = residual.replace("s", "ks")
		# ---------------------------------------------------

		return Xr_num, Yr_num, pq_num, residual


	@staticmethod 
	def left_align(top, bottom) :
		
		# -- Adaptation for washing chemistry with linkers --
		# linker-start behaves the same as start
		# so, ks is replaced with s here, and at the end, s is replaced with ks, to keep the alignment working
		ks_replaced_by_s = False
		if ("ks" in top) or ("ks" in bottom) :
			top = top.replace("ks", "s")
			bottom = bottom.replace("ks", "s")
			ks_replaced_by_s = True
		# ---------------------------------------------------

		# for rules C3 and C4
		# left-align top and bottom (strand displacement from beginning)
		t = len(top); b = len(bottom)
		if t > b :
			bottom = bottom + "-"*(t-b)
		elif b > t :
			top = top + "-"*(b-t)

		# scan from left, making pairs
		Xr_num = 0
		Yr_num = 0		
		pq_num = 0; sp_num = 0

		for i in range(0, len(top)) :
			if top[i] == "X" and bottom[i] == "r" :
				Xr_num += 1
			elif top[i] == "Y" and bottom[i] == "r" :
				Yr_num += 1
			elif top[i] == "p" and bottom[i] == "q" :
				pq_num += 1
			elif top[i] == "p" and bottom[i] == "s" :
				sp_num += 1

		residual = ""
		if t > b :
			residual = top[-(t-b):]
			# which can also start with an sp pair
			if sp_num > 0 :
				residual = "sp" + residual
		elif b > t :
			residual = bottom[-(b-t):]
		elif b == t :
			if sp_num > 0 :
				residual = "sp"

		# -- Adaptation for washing chemistry with linkers --
		if ks_replaced_by_s :
			residual = residual.replace("s", "ks")
		# ---------------------------------------------------				

		return Xr_num, Yr_num, pq_num, residual


	@staticmethod 
	def reaction_products(Xr_num, Yr_num, pq_num, residual) :
		# format reaction products into a list

		Xr = "IXrI+" * Xr_num
		Yr = "IYrI+" * Yr_num
		pq = "IpqI+" * pq_num

		all_helix_products = Xr + Yr + pq
		all_helix_products = all_helix_products[:-1]

		# residual may exist or not
		if residual == "" :
			return all_helix_products.split("+")
		else :
			# residual polymer exists
			return (all_helix_products + "+" + residual).split("+")


					



# involves X/Y and r hybridisation at right hand end. This is A domain hybridisation
class C1(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :

		fit1 = (k.endswith("X") or k.endswith("Y")) and (len(k) >= 2) and (l.endswith("r")) and (len(l) >= 2)
		fit2 = (l.endswith("X") or l.endswith("Y")) and (len(l) >= 2) and (k.endswith("r")) and (len(k) >= 2)

		if fit1 :
			Xr_num, Yr_num, pq_num, residual = ChainOutcome.right_align(k, l)
			yield self.Transition([k,l], ChainOutcome.reaction_products(Xr_num, Yr_num, pq_num, residual), constants.C_KA)

		elif fit2 :
			Xr_num, Yr_num, pq_num, residual = ChainOutcome.right_align(l, k)
			yield self.Transition([k,l], ChainOutcome.reaction_products(Xr_num, Yr_num, pq_num, residual), constants.C_KA)


# involves p and q hybridisation at right hand end. This is a BC domain hybridisation
class C2(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :

		fit1 = k.endswith("p") and (len(k) >= 2) and l.endswith("q") and len(l) >= 2
		fit2 = l.endswith("p") and (len(l) >= 2) and k.endswith("q") and len(k) >= 2

		if fit1 :
			Xr_num, Yr_num, pq_num, residual = ChainOutcome.right_align(k, l)
			yield self.Transition([k,l], ChainOutcome.reaction_products(Xr_num, Yr_num, pq_num, residual), constants.C_KBC)

		elif fit2 :
			Xr_num, Yr_num, pq_num, residual = ChainOutcome.right_align(l, k)
			yield self.Transition([k,l], ChainOutcome.reaction_products(Xr_num, Yr_num, pq_num, residual), constants.C_KBC)			


# involves X/Y and r hybridisation at left hand end. This is a BC domain hybridisation
class C3(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :

		fit1 = (k.startswith("X") or k.startswith("Y")) and (len(k) >= 2) and l.startswith("r") and (len(l) >= 2)
		fit2 = (l.startswith("X") or l.startswith("Y")) and (len(l) >= 2) and k.startswith("r") and (len(k) >= 2)

		if fit1 :
			Xr_num, Yr_num, pq_num, residual = ChainOutcome.left_align(k, l)
			yield self.Transition([k,l], ChainOutcome.reaction_products(Xr_num, Yr_num, pq_num, residual), constants.C_KBC)			

		elif fit2 :
			Xr_num, Yr_num, pq_num, residual = ChainOutcome.left_align(l, k)			
			yield self.Transition([k,l], ChainOutcome.reaction_products(Xr_num, Yr_num, pq_num, residual), constants.C_KBC)


# involves p and q hybridisation at left hand end. This is an A domain hybridisation
class C4(TransitionRule) :
	Transition = MassAction

	def novel_reactions(self, k, l) :

		fit1 = k.startswith("p") and (len(k) >= 2) and l.startswith("q") and (len(l) >= 2)
		fit2 = l.startswith("p") and (len(l) >= 2) and k.startswith("q") and (len(k) >= 2)

		if fit1 :
			Xr_num, Yr_num, pq_num, residual = ChainOutcome.left_align(k, l)
			yield self.Transition([k,l], ChainOutcome.reaction_products(Xr_num, Yr_num, pq_num, residual), constants.C_KA)			

		if fit2 :
			Xr_num, Yr_num, pq_num, residual = ChainOutcome.left_align(l, k)
			yield self.Transition([k,l], ChainOutcome.reaction_products(Xr_num, Yr_num, pq_num, residual), constants.C_KA)			





