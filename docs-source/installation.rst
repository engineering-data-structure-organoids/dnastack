Installation
============

Typical installation time is 10 minutes.

Mac OS
------

Installation is via the terminal. Open Terminal.app (in Applications/Utilities).

1. Change to the directory where you want to put the source code of **dnastack**. For example: ::

	cd /Users/myname

2. Download a copy of the source code, using the following command: ::

	git clone https://bitbucket.org/engineering-data-structure-organoids/dnastack.git

.. note:: Running the ``git`` command for the first time on Mac OS may require you to download developer tools. If these are not installed, a dialog box should prompt you to install them.

3. Change to the newly created ``dnastack`` directory: ::

	cd dnastack

4. Make a new python3 virtual environment, enable it and install the required packages: ::

	python3 -m venv venv
	source venv/bin/activate
	pip install --upgrade pip
	pip install -r requirements.txt	

5. Run the program by following the :ref:`Quick Demo` page, or the more detailed instructions on the :ref:`Running a Simulation` page.

6. Done? Leave the virtual environment by typing: ::

	deactivate

If you would like to make virtual gel images of simulation output (see page :ref:`Virtual Gel Image`), follow the additional steps below to install ImageMagick.

7. Install the Mac OS package manager Homebrew. This is necessary for installing ImageMagick in the next step. As described at https://brew.sh, install Homebrew by running command: ::

	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"

8. Install ImageMagick using homebrew: ::

	brew install imagemagick



Linux Ubuntu
------------

Installation is via the terminal.

1. Update your repositories and install git: ::

	sudo apt-get update
	sudo apt install git

2. Install pip for Python3: ::

	sudo apt-get install build-essential libssl-dev libffi-dev python-dev
	sudo apt install python3-pip

3. Use pip to install virtualenv: ::

	sudo pip3 install virtualenv

4. If you would like to make virtual gel images of simulation output (see page :ref:`Virtual Gel Image`), install ImageMagick: ::

	sudo apt install imagemagick

5. Follow the same instructions as for Mac OS above, up to Mac OS step 6.


Windows 10
----------

We recommend installing a Linux Ubuntu environment on Windows, and then following the Linux installation instructions above. A Linux environment can be installed by using the Windows Subsystem for Linux (available on 64-bit versions of Windows 10). Alternatively, using Ubuntu under `VirtualBox <https://www.virtualbox.org/>`_ is another option.


