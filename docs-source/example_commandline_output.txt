------------------------------------------------------
STOCAL stochastic simulation of stack washing
spX
example lane 3

PHI_0 = 33% volume fraction of supernatant species SURVIVE each wash via non-specific bead binding initially, when normalised bead mass = 1.0
MU  = 10% of remaining beads LOST on each wash
VOL = 1.50000e-13 litre (100nM = 9033 particles, 300nM = 27099 particles)

	Bead mass remaining: 1.00 
	Incubating linker (200 nM) with beads, waiting sufficient time to get 100% binding
	--- Wash ---
	Bead mass remaining: 0.90 
	Adding s (300 nM) to reaction, waiting sufficient time to get 100% reaction completion
	--- Wash ---
	Bead mass remaining: 0.81 
	Adding p (300 nM) to reaction, waiting 30.00 mins
	(Recording chemistry state, supernatant state, and supernatant state after a releaser)
	--- Wash ---
	Bead mass remaining: 0.73 
	Adding X (300 nM) to reaction, waiting 30.00 mins
	(Recording chemistry state, supernatant state, and supernatant state after a releaser)

Done.


------------------------------------------------------
Final state: (not directly observable by electrophoresis)
8 nM		ks			725
0 nM		s			30
0 nM		p			4
6 nM		ksp			576
0 nM		sp			33
123 nM		X			11126
119 nM		kspX			10772
0 nM		Xp			52
0 nM		pX			14
6 nM		spX			569
10 nM		XpX			982
0 nM		kspXp			56
10 nM		kspXpX			951
0 nM		spXp			2
0 nM		spXpX			58
0 nM		pXpX			1
0 nM		XpXp			2
0 nM		kspXpXpX			75
0 nM		XpXpX			72
0 nM		kspXpXp			8
0 nM		spXpXpX			4
0 nM		XpXpXpXpX			1
0 nM		XpXpXpX			5
0 nM		kspXpXpXpX			5
0 nM		kspXpXpXpXpXpX			1

Supernatant of final state:
0 nM		s			30
0 nM		p			4
0 nM		sp			33
123 nM		X			11126
0 nM		Xp			52
0 nM		pX			14
6 nM		spX			569
10 nM		XpX			982
0 nM		spXp			2
0 nM		spXpX			58
0 nM		pXpX			1
0 nM		XpXp			2
0 nM		XpXpX			72
0 nM		spXpXpX			4
0 nM		XpXpXpXpX			1
0 nM		XpXpXpX			5

Supernatant state AFTER further wash, and releaser applied: (released stacks present)
7 nM		s			658
3 nM		sp			320
110 nM		spX			10001
0 nM		spXp			26
10 nM		spXpX			910
0 nM		spXpXpX			70
0 nM		spXpXp			6
0 nM		spXpXpXpX			4
24 nM		X			2194
0 nM		Xp			6
2 nM		XpX			195
0 nM		XpXpX			16
0 nM		XpXpXpX			1
68 nM		z			6219