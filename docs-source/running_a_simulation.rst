Running a Simulation
====================

This page contains more detailed instructions about running a DNA stack simulation.

Before running any simulation, change to the ``dnastack`` directory and activate the Python virtual environment: ::

	cd dnastack
	source venv/bin/activate

General Syntax
--------------

To run a stochastic simulation of gel "example", lane 2, the command is: ::

	python3 washing.py example 2

The name of the gel is specified first, followed by the lane on the gel to simulate.

The output of the simulator is a pickle file placed in the ``results`` directory at the end of the simulation (in the above case, ``sim_example_2.pkl`` will be created). This pickle file contains the model state after *each* brick is added (see :ref:`Output Pickle File` for details).

When simulations of one or more lanes of a gel have been executed, a virtual image of the gel can be produced as described in :ref:`Virtual Gel Image`.

.. note:: Stochastic simulation proceeds via the Gillespie Direct algorithm, which explicitly simulates every reaction. Simulations where many strands are added one after the other can take significant time to execute (hours, or sometimes days). The system volume in ``constants.py`` is set to 1.5e-13 litres (0.15 picolitres) to keep the species populations at acceptable levels for computationally tractable simulations.

Defining a Gel
--------------

To define a gel to simulate, a simple python function in the file ``constants.py`` must be created that has the name of the gel.

The function ``example()`` in ``constants.py``, for instance, specifies a demo gel called "example". The code inside the function describes what DNA strands are put in each of the lanes of the gel. Looking at the code for lane 2, we see: ::

	def example(lane) :
	  if(lane == 2) :
	    bricks_add_order    =   ['s',    'p']
	    bricks_add_conc     =   [300e-9, 300e-9]
	    bricks_wait_time    =   [    30*60,   30*60]

This code means that in lane 2 of "example" gel:

* a **start** strand is initially added at 300nM...
* then 30*60 seconds are waited (30 minutes)...
* after which a **push** strand is added at 300nM.. 
* followed by a wait of a further 30 minutes.

Specifying Washing Parameters
-----------------------------

If not specified, the simulation uses the default ``MU`` and ``PHI`` washing parameters, specified in ``constants.py``. 

- ``PHI`` is the initial fraction of supernatant species transferred through the wash to the next reaction (between 0 and 1, inclusive).
- ``MU`` is the fraction of beads lost on each wash (between 0 and 1, inclusive)

For convenience, these two parameters can also be specified on the command line, overriding the default values in ``constants.py``. For example, to specify ``PHI`` = 0.2, ``MU`` = 0.1 use: ::

	python3 washing.py example 2 0.2 0.1






