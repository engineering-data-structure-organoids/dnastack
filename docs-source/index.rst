dnastack Documentation
======================

.. image:: stack.png
  :width: 600
  :alt: Image of a DNA stack with 3 signals

This documentation describes how to install and run **dnastack**, a stochastic model of the the DNA stack chemistry described in paper: A Last-In First-Out Stack Data Structure Implemented in DNA, Lopiccolo and Shirt-Ediss, et al., Nature Comms 12, 4861 (2021) `doi:10.1038/s41467-021-25023-6 <https://doi.org/10.1038/s41467-021-25023-6>`_.

**dnastack** is written in Python 3 and makes use of the `stocal <https://pypi.org/project/stocal/>`_ simulation engine for polymer chemistries that have a variable size state space. The simulation outputs the predicted concentration of each DNA polymer species at the end of an experiment and these concentrations can be further displayed as virtual polyacrylamide gel images.

The source code for the model is available on `Bitbucket <https://bitbucket.org/engineering-data-structure-organoids/dnastack>`_, released under an MIT Licence.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   installation
   demo
   running_a_simulation
   output_pickle
   virtual_gel