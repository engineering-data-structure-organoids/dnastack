dnastack README
---------------

Description:

	Stochastic chemical kinetics model of DNA stack polymer chemistry, written in Python 3. 

Installation and usage instructions are provided at:

	https://dnastack.readthedocs.io

	and also locally, in the folder:

	docs/index.html

Author:

	Ben Shirt-Ediss, 2019-2020

Licence:

	This work is provided under an MIT licence