Quick Demo
==========

To run a quick demo to test that the installation works:

1. Change to the ``dnastack`` directory and activate the Python virtual environment (if not already activated): ::

	cd dnastack
	source venv/bin/activate

2. Then run a stochastic simulation of adding DNA strands in order *start* - *push* - *X* (each at 300nM concentration and separated by a wait time of 30 minutes) by typing: ::

	python3 washing.py example 3

After around 1 minute of computation, output similar to the following should be produced:

.. literalinclude:: example_commandline_output.txt

The numerical data reported are the final concentrations (and particle numbers) of all species in the stack chemistry. Note that the output you obtain will probably vary slightly due to the simulation being stochastic. 

See the :ref:`Running a Simulation` page for more detailed running instructions. 

Also, see the :ref:`Virtual Gel Image` page for how to turn numerical simulation results into a more intuitive virtual polyacrylamide gel image.



 
