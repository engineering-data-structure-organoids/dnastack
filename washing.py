"""Simulates a single DNA stack experiment with washing (a single gel lane)

Supernatant and supernatant-after-releaser are recorded, following the addition of each brick
Kinetic trajectory is NOT recorded for memory reasons

STOCAL is used for stochastic simulation.

Ben Shirt-Ediss, May/June 2019, Finalised October 2019
"""

import constants
from stocal import *
from washing_rules import *

import sys
import matplotlib.pyplot as plt
import pickle




#
#
# STOCAL stochastic process (20 reaction rules)
#
#

process = Process( rules = [ LH1(), LH2(), LH3(), H1(), H2(), LH4(), LH5(), LH6(), SD1(), SD2(), SD3(), SD4(), SD5(), SD6(), SD7(), SD8(), C1(), C2(), C3(), C4() ] )















#
#
# Sampler of STOCAL stochastic process over a set TIME WINDOW.
#
#

def sample_window(startstate, t_start, t_max, rule_name, rule_firing) :
	"""
	Modifies STOCAL sampler.
	If sampler did not reach end time t_max, start state is copied to end state.
	Returns the end state of the system at t_max. Updates the rule firing totals.
	"""

	# ---- sample trajectory, the standard stocal way -----
	trajectory = process.sample(startstate, tstart=t_start, tmax=t_max)

	samples_taken = False

	for dt, transitions in trajectory :			# loop to update the state of trajectory
		
		# rule fire logging turned off for speed up		
		"""
		# record which rule just fired
		for tr in transitions :					# normally, just one transition fired
			r = str(tr.rule).split()[0].split(".")[1] 	# parse rule name from e.g. <washing_rules.LH1 object at 0x1005b71d0>
			i = rule_name.index(r)
			rule_firing[i] += 1
		"""

		samples_taken = True
	# -----------------------------------------------------

	endstate = {}

	# If no samples possible between start and end time 
	# (either [a] because system started in an inert configuration, or [b] time to first transition went over the end time), 
	# then copy state at start time, to state at end time
	if not samples_taken :
		
		for species in startstate :
			endstate[species] = startstate[species]

	else :

		for species in trajectory.state :
			endstate[species] = trajectory.state[species]

	return endstate
















#
#
# Record state, its supernatant, and supernatant of a further releaser step
#
#
#

def record_final_and_supernatant_states(time, endstate, PHI, MU, bead_mass_normalised, rule_name, rule_firing, simdata, bricknum, brick) :
	"""Returns a positive integer of the brick number, if sim stopped prematurely
	"""

	premature_stop_bricknum = -1

	# --------------------------------------------
	# final state and its supernatant
	# --------------------------------------------

	finalstate = {}			# final state of everything in the tube (particle number and nM concentration)
	supernatant = {}		# final state of free-floating supernatant only 

	for species in endstate :
		nM = ( endstate[species] / (constants.NA * constants.VOL) ) * 1e9
		finalstate[species] = ( endstate[species], nM )

		if (species.startswith("k") or species.endswith("ks")) :
			pass 	# bead bound species do not pass to supernatant
		else :
			supernatant[species] = ( endstate[species], nM )

	simdata["finalstate%d" % (bricknum)] = finalstate							
	simdata["supernatant%d" % (bricknum)] = supernatant 						# supernatant with no final releaser applied


	#  ------ Speed up for creating paper figures -------
	# prematurely stop if read was the brick, and the popped signals Xr and Yr in supernatant
	# cannot be distinguished (conc diff less than 10nM)
	if constants.STOP_WHEN_POPPED_SIGNALS_INDISTINGUISHABLE == True and brick == "r" :
		
		# get nM conc of Xr and Yrin supernatant
		Xr = 0
		Yr = 0

		if supernatant.get("IXrI", -1) != -1 :
			Xr = supernatant["IXrI"][1]

		if supernatant.get("IYrI", -1) != -1 :
			Yr = supernatant["IYrI"][1]

		diff = abs(Xr - Yr)
		diff_sufficient = (diff >= constants.EPSILON_XY)

		if not diff_sufficient :
			premature_stop_bricknum = bricknum
	# ----------------------------------------------------


	# --------------------------------------------
	# apply releaser
	# --------------------------------------------
	
	startstate = {}	

	# WASHING STEP

	# remaining bead mass after the wash
	bead_mass_normalised = bead_mass_normalised * (1 - MU)

	for species in endstate :
		surviving_copies = 0
		if species.startswith("k") or species.endswith("ks") :	# tethered sigma and tau stacks, respectively
			surviving_copies = int( endstate[species] * (1 - MU) )	# bead loss
		else :
			surviving_copies = int( endstate[species] * (PHI * bead_mass_normalised) )		# wash out of supernatant, leaving residual
				
		if surviving_copies > 0 :
			startstate[species] = surviving_copies

	# RELEASER REACTION
	# leave the system reacting for the time period where the releaser is introduced
	# the action of the releaser strand displacing the bound stacks is NOT included during these dynamics
	# However, the releaser strand displacement does not affect the dynamics of other species 
	# as all it does is make ks stacks turn into s stacks -- and both types of stack have the same reactivity.

	rule_firing_copy = list(rule_firing)	# make a shallow copy of the list, to update with fire counts, so main list not altered

	endstate = sample_window(startstate, time, time + constants.RELEASER_WAIT_TIME, rule_name, rule_firing_copy)

	# ADD RELEASER AND MAKE CORRECTED FINAL END STATE
	z_particle_number = int(constants.RELEASER_CONC * constants.NA * constants.VOL)	# releaser

	corrected_endstate = {}

	# first pass; convert all bead-bound sigma and tau stacks, to free floating released stacks
	for species in endstate :
		if species == "k" :
			# bead linker by itself
			z_particle_number -= endstate[species] 						# releaser reacts with it, and forms kz double helix bound to a bead
				# no product is released			

		elif species.startswith("k") :	# tethered sigma stack, with linker-start at the beginning
			# bead linker, plus attached strands
			z_particle_number -= endstate[species] 						# releaser reacts with it, and forms kz double helix bound to a bead
			corrected_endstate[species[1:]] = endstate[species]			# plus released product

		elif species.endswith("ks") :	# tethered tau stack, with linker-start at the end
			z_particle_number -= endstate[species] 						# releaser reacts with it, and forms kz double helix bound to a bead
			corrected_endstate[species[:-2] + "s"] = endstate[species]	# plus released product

	# note: releaser must be in excess
	# because ALL species attached to beads are released by the releaser.
	# If releaser is not in excess, which species are released, and which not? How do you decide without a kinetics model?
	if z_particle_number < 0 :
		print("Warning: Releaser is not in sufficient excess")
		quit()

	# second pass; carry over all other species to corrected end state
	for species in endstate :
		if species.startswith("k") or species.endswith("ks") :
			pass	# already carried over
		else :
			if corrected_endstate.get(species, -1) == -1 :
				# this species not in corrected end state - make new entry for it
				corrected_endstate[species] = endstate[species]
			else :
				# this species IS ALREADY in corrected endstate - add to existing particle number
				# (this happens when a released species is the same as a species already in the supernatant)
				corrected_endstate[species] += endstate[species]

	# add surplus releaser complex to end state
	corrected_endstate["z"] = z_particle_number
	
	# note: linker-releaser dsDNA helices stay **attached** to the beads, and never make it into the supernatant

	# --------------------------------------------
	# supernatant after releaser
	# --------------------------------------------

	supernatant_releaser = {}

	for species in corrected_endstate :
		nM = ( corrected_endstate[species] / (constants.NA * constants.VOL) ) * 1e9
		supernatant_releaser[species] = ( corrected_endstate[species], nM )

	simdata["supernatant_releaser%d" % (bricknum)] = supernatant_releaser		# supernatant with final wash then releaser applied
	simdata["rule_firing%d" % (bricknum)] = rule_firing_copy

	return premature_stop_bricknum














#
#
# Main simulation
#
#

def simulate(gelname, lanenum, phi = None, mu = None, spXY = None, rq = None, wt = None, eta = None, dir1 = None, dir2 = None) :

	PHI = constants.PHI; MU = constants.MU
	if phi != None and mu != None :
		PHI = phi; MU = mu

	method_to_call = getattr(constants, gelname)	

	bricks_add_order, bricks_add_conc, bricks_wait_time = method_to_call(lane=lanenum)	
	
	if wt != None :
		bricks_add_order, bricks_add_conc, bricks_wait_time = method_to_call(lane=lanenum, spXY=spXY, rq=rq, wt=wt)
	elif eta != None :
		bricks_add_order, bricks_add_conc, bricks_wait_time = method_to_call(lane=lanenum, eta=eta)

	# *** (just for counting which rules fired)
	rule_name = ["LH1", "LH2", "LH3", "H1", "H2", "LH4", "LH5", "LH6", 
					"SD1", "SD2", "SD3", "SD4", "SD5", "SD6", "SD7", "SD8",
					"C1", "C2", "C3", "C4"]
	rule_firing = [0] * len(rule_name) 	# to count how many times each of the rules fires during a run
	# ***

	simdata = {}

	time = 0
	endstate = None
	bead_mass_normalised = 1.0 			# represents current bead mass / initial bead mass 	(Bm/Bm0)

	premature_stop_bricknum = -1		# becomes positive at the read bricknum where 
										# Xr and Yr signals in supernatant are not distinguishable

	# checks
	if bricks_add_order[0] != "s" :
		print("Invalid washing experiment. First brick added to experiment must be start.")
		quit()

	print("")
	print("------------------------------------------------------")
	print("STOCAL stochastic simulation of stack washing")
	print("%s" % ("".join(bricks_add_order)))
	print("%s lane %d" % (gelname, lanenum))
	print("")
	print("PHI_0 = %d%% volume fraction of supernatant species SURVIVE each wash via non-specific bead binding initially, when normalised bead mass = 1.0" % (PHI * 100))
	print("MU  = %d%% of remaining beads LOST on each wash" % (MU * 100))
	print("VOL = %.5e litre (100nM = %d particles, 300nM = %d particles)" % (constants.VOL, int(100e-9 * constants.NA * constants.VOL), int(300e-9 * constants.NA * constants.VOL)))
	print("")

	for i, brick in enumerate(bricks_add_order) :
		if i == 0 :
			# NOT SIMULATED - mixing linker with beads, then start with linker
			print("\tBead mass remaining: %1.2f " % (bead_mass_normalised))
			print("\tIncubating linker (%2.0f nM) with beads, waiting sufficient time to get 100%% binding" % (constants.LINKER_CONC*1e9))
			print("\t--- Wash ---")
			print("\tBead mass remaining: %1.2f " % (bead_mass_normalised * (1 - MU)))
			print("\tAdding s (%2.0f nM) to reaction, waiting sufficient time to get 100%% reaction completion" % (bricks_add_conc[0]*1e9))
			print("\t--- Wash ---")

			endtime = time + bricks_wait_time[0]	# start wait time is normally 30 minutes to allow 100% hybridisation to linker
			time = endtime

		elif i == 1 :			
			# set up initial condition for simulation
			startstate = {}

			n_k0 = constants.LINKER_CONC * constants.NA * constants.VOL		# number of linker strands added
			n_s0 = bricks_add_conc[0] * constants.NA * constants.VOL 		# number of start strands added

			n_s_surplus = n_s0 - ((1 - MU) * n_k0)				# number of start monomers in excess, once all have bound with linkers (negative means linkers are instead in excess)

			# remaining bead mass after 2 washes
			bead_mass_normalised = bead_mass_normalised * (1 - MU) * (1 - MU)

			if n_s_surplus >= 0 :
				# (1) start in excess of linker
				
				#n_k = 0

				n_ks = int( (1 - MU) * (1 - MU) * n_k0 )
				if n_ks > 0 :
					startstate["ks"] = n_ks

				n_s = int( (PHI * bead_mass_normalised * n_s_surplus) )
				if n_s > 0 :
					startstate["s"] = n_s

			else :
				# (2) linker in excess of start

				n_k = int( -1 * (1 - MU) * n_s_surplus )
				if n_k > 0 :
					startstate["k"] = n_k 		# these excess linkers will remain non-reacted to end of sim. They only bind start.

				n_ks = int( (1 - MU) * n_s0 )
				if n_ks > 0 :
					startstate["ks"] = n_ks

				#n_s = 0

			# add the next brick after start, and simulate
			brick = bricks_add_order[1]
			brick_particle_number = bricks_add_conc[1] * constants.NA * constants.VOL
			if int(brick_particle_number) > 0 :
				startstate[brick] = int(brick_particle_number)

			endtime = time + bricks_wait_time[1]
			print("\tBead mass remaining: %1.2f " % (bead_mass_normalised))
			print("\tAdding %s (%2.0f nM) to reaction, waiting %2.2f mins" % (brick, bricks_add_conc[1]*1e9, (endtime-time)/60))

			# REACTION
			endstate = sample_window(startstate, time, endtime, rule_name, rule_firing)

			time = endtime

			# record chemistry state, supernatant state, and supernatant state were a further releaser applied
			print("\t(Recording chemistry state, supernatant state, and supernatant state after a releaser)")

			premature_stop_bricknum = record_final_and_supernatant_states(time, endstate, PHI, MU, bead_mass_normalised, rule_name, rule_firing, simdata, i, brick)
			
			if premature_stop_bricknum > -1 :
				break

		else :
			# adding 2nd, 3rd, 4th... brick after start

			startstate = {}

			print("\t--- Wash ---")

			# WASHING STEP

			# remaining bead mass AFTER the wash
			bead_mass_normalised = bead_mass_normalised * (1 - MU)

			for species in endstate :
				surviving_copies = 0
				if species.startswith("k") or species.endswith("ks") :	# tethered sigma and tau stacks, respectively
					surviving_copies = int( endstate[species] * (1 - MU) )	# bead loss
				else :
					surviving_copies = int( endstate[species] * (PHI * bead_mass_normalised) )		# wash out of supernatant, leaving residual
				
				if surviving_copies > 0 :
					startstate[species] = surviving_copies

			# ADD NEW BRICK
			brick = bricks_add_order[i]
			brick_particle_number = bricks_add_conc[i] * constants.NA * constants.VOL
			if int(brick_particle_number) > 0 :
				startstate[brick] = int(brick_particle_number)

			endtime = time + bricks_wait_time[i]

			print("\tBead mass remaining: %1.2f " % bead_mass_normalised)
			print("\tAdding %s (%2.0f nM) to reaction, waiting %2.2f mins" % (brick, bricks_add_conc[i]*1e9, (endtime-time)/60))

			# REACTION
			endstate = sample_window(startstate, time, endtime, rule_name, rule_firing)

			time = endtime

			# record chemistry state, supernatant state, and supernatant state were a further releaser applied
			print("\t(Recording chemistry state, supernatant state, and supernatant state after a releaser)")

			premature_stop_bricknum = record_final_and_supernatant_states(time, endstate, PHI, MU, bead_mass_normalised, rule_name, rule_firing, simdata, i, brick)
			
			if premature_stop_bricknum > -1 :
				break


	print("")
	print("Done.")
	print("")

	# --------------------------------------------
	# save sim data to pickle file, labelled with gel lane number
	# --------------------------------------------
	
	simdata["bricks_add_order"] = bricks_add_order
	simdata["bricks_add_conc"] = bricks_add_conc
	simdata["bricks_wait_time"] = bricks_wait_time
	simdata["rule_name"] = rule_name	

	filename = ""
	if dir1 != None and dir2 != None :
		filename = "%s/%s/sim_%s_%d.pkl" % (dir1, dir2, gelname, lanenum)
	else :
		filename = "results/sim_%s_%d.pkl" % (gelname, lanenum)
	
	f = open(filename, "wb")
	pickle.dump(simdata, f)
	f.close()

	# --------------------------------------------
	# open simdata pkl file, and report final state after all bricks added
	# --------------------------------------------
	f = open(filename, "rb")
	simdata = pickle.load(f)
	f.close()

	if premature_stop_bricknum > -1 :
		# sim stopped prematurely

		supernatant = simdata["supernatant%d" % premature_stop_bricknum]

		print("")
		print("------------------------------------------------------")		
		print("Simulation stopped prematurely, as Xr and Yr signals indistinguishable following a read")

		print("")
		print("Supernatant of final state:")
		for species in supernatant :
			P, nM = supernatant[species]
			print("%d nM\t\t%s\t\t\t%d" % (nM, species, P))

		print("")

		for key in simdata :
			print(key)

	else :
		# sim went right to the last brick added

		bricks_added_after_start = len(simdata["bricks_add_order"]) - 1
		finalstate = simdata["finalstate%d" % bricks_added_after_start]
		supernatant = simdata["supernatant%d" % bricks_added_after_start]
		supernatant_releaser = simdata["supernatant_releaser%d" % bricks_added_after_start]
		rule_name = simdata["rule_name"]
		rule_firing = simdata["rule_firing%d" % bricks_added_after_start]

		print("")
		print("------------------------------------------------------")
		print("Final state: (not directly observable by electrophoresis)")
		for species in finalstate :
			P, nM = finalstate[species]
			print("%d nM\t\t%s\t\t\t%d" % (nM, species, P))

		print("")
		print("Supernatant of final state:")
		for species in supernatant :
			P, nM = supernatant[species]
			print("%d nM\t\t%s\t\t\t%d" % (nM, species, P))

		print("")
		print("Supernatant state AFTER further wash, and releaser applied: (released stacks present)")
		for species in supernatant_releaser :
			P, nM = supernatant_releaser[species]
			print("%d nM\t\t%s\t\t\t%d" % (nM, species, P))

		#print("")
		#print("Number of times each rule fired over entire simulation")
		#for i, rn in enumerate(rule_name) :
		#	print("%s\t\t%d" % (rn, rule_firing[i]))
		
		print("")















#
#
# Run a simulation
#
#

if __name__ == "__main__" :
	"""
	Example:

	$ washing.py gel33 3 (phi value) (mu value)

	Simulates lane 3 of gel 33 (gel 33 is defined in constants.py).
	If values of phi and mu are not specified as arguments, the values specified in constants.py are used

	The output data file is a PKL file, containing a dictionary "simdata" with the following format:

	simdata["bricks_add_order"]		- list of bricks added, in order
	simdata["bricks_add_conc"]		- list of brick concentrations
	simdata["bricks_wait_time"]		- list of times waited after each brick added
	simdata["rule_name"]			- list of reaction rule names

	simdata["finalstateN"] 			- complete final state of chemistry after Nth brick after start brick added, and wait time elapsed
	simdata["supernatantN"]			- supernatant (free floating) species in chemistry after Nth brick after start brick added, and wait time elapsed
	simdata["supernatant_releaserN"]- supernatant (free floating) species in chemistry after Nth brick after start brick added, wait time elapsed, releaser added, releaser wait time elapsed
	simdata["rule_firingN"] 		- list of rule firing counts in simulation up to Nth brick after start, including rules firing during releaser wait time phase
	"""

	
	if len(sys.argv) >= 3 :

		gelname = str(sys.argv[1])
		lanenum = int(sys.argv[2])

		if len(sys.argv) == 3 :
			# use default phi and mu value in constants.py, and put results in results/ directory
			simulate(gelname, lanenum)

		elif len(sys.argv) == 5 :
			# use user specified phi and mu value, and put results in results/ directory
			user_phi = float(sys.argv[3])
			user_mu = float(sys.argv[4])

			simulate(gelname, lanenum, phi = user_phi, mu = user_mu)

		elif len(sys.argv) == 7 :
			# use user specified phi and mu value, and put results in dir1/dir2 directory
			user_phi = float(sys.argv[3])
			user_mu = float(sys.argv[4])

			user_dir1 = str(sys.argv[5])
			user_dir2 = str(sys.argv[6])

			simulate(gelname, lanenum, phi = user_phi, mu = user_mu, dir1 = user_dir1, dir2 = user_dir2)			
		
		elif len(sys.argv) == 8:
			# (Note: this is NOT a normal use case, its used ONLY for computing fig4 in paper)
			# use user specified phi, mu values, and pipetting noise. Put results in dir1/dir2 directory
			user_phi = float(sys.argv[3])
			user_mu = float(sys.argv[4])

			user_eta = float(sys.argv[5])

			user_dir1 = str(sys.argv[6])
			user_dir2 = str(sys.argv[7])

			if gelname == "fig5d" :
				simulate(gelname, lanenum, phi = user_phi, mu = user_mu, eta = user_eta, dir1 = user_dir1, dir2 = user_dir2)			

		elif len(sys.argv) == 10 :
			# (Note: this is NOT a normal use case, its used ONLY for computing fig4 in paper)
			# use user specified phi, mu values, and brick concs and wait times. Put results in dir1/dir2 directory
			user_phi = float(sys.argv[3])
			user_mu = float(sys.argv[4])

			user_spXY = float(sys.argv[5])
			user_rq = float(sys.argv[6])
			user_wt = float(sys.argv[7])

			user_dir1 = str(sys.argv[8])
			user_dir2 = str(sys.argv[9])

			if gelname == "fig5c" :
				simulate(gelname, lanenum, phi = user_phi, mu = user_mu, spXY = user_spXY, rq = user_rq, wt = user_wt, dir1 = user_dir1, dir2 = user_dir2)

		else :
			print("Supply either:")
			print("- two arguments, e.g. washing.py gel33 3")
			print("- six arguments e.g. washing.py gel33 3 (phi value) (mu value) (results dir) (results subdir)")

	else :
			print("Supply either:")
			print("- two arguments, e.g. washing.py gel33 3")
			print("- six arguments e.g. washing.py gel33 3 (phi value) (mu value) (results dir) (results subdir)")
 